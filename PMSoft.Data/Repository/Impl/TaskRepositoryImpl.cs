﻿using PMSoft.Data.Infrastructure;
using PMSoft.Models;

namespace PMSoft.Data.Repository.Impl
{
    /// <summary>
    /// Default implementation of <see cref="ITaskRepository"/>
    /// </summary>
    class TaskRepositoryImpl : RepositoryBase<Task>, ITaskRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TaskRepositoryImpl"/> class.
        /// </summary>
        /// <param name="contextFactory">The context factory.</param>
        public TaskRepositoryImpl(IContextFactory contextFactory)
            : base(contextFactory)
        {
        }
    }
}
