﻿using PMSoft.Data.Infrastructure;
using PMSoft.Models;

namespace PMSoft.Data.Repository.Impl
{
    /// <summary>
    /// Default implementation of <see cref="IActivityRepository"/>
    /// </summary>
    class ActivityRepositoryImpl : RepositoryBase<Activity>, IActivityRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ActivityRepositoryImpl"/> class.
        /// </summary>
        /// <param name="contextFactory">The context factory.</param>
        public ActivityRepositoryImpl(IContextFactory contextFactory)
            : base(contextFactory)
        {
        }
    }
}
