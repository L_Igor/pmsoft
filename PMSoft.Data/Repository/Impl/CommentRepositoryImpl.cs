﻿using PMSoft.Data.Infrastructure;
using PMSoft.Models;

namespace PMSoft.Data.Repository.Impl
{
    /// <summary>
    /// Default implementation of <see cref="ICommentRepository"/>
    /// </summary>
    class CommentRepositoryImpl : RepositoryBase<Comment>, ICommentRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CommentRepositoryImpl"/> class.
        /// </summary>
        /// <param name="contextFactory">The context factory.</param>
        public CommentRepositoryImpl(IContextFactory contextFactory)
            : base(contextFactory)
        {
        }
    }
}
