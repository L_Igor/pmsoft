﻿using PMSoft.Data.Infrastructure;
using PMSoft.Models;

namespace PMSoft.Data.Repository.Impl
{
    /// <summary>
    /// Default implementation of <see cref="IUserRepository"/>
    /// </summary>
    class UserRepositoryImpl : RepositoryBase<ApplicationUser>, IUserRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UserRepositoryImpl"/> class.
        /// </summary>
        /// <param name="contextFactory">The context factory.</param>
        public UserRepositoryImpl(IContextFactory contextFactory)
            : base(contextFactory)
        {
        }
    }
}
