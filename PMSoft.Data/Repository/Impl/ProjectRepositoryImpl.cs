﻿using PMSoft.Data.Infrastructure;
using PMSoft.Models;

namespace PMSoft.Data.Repository.Impl
{
    /// <summary>
    /// Default implementation of <see cref="IProjectRepository"/>
    /// </summary>
    class ProjectRepositoryImpl : RepositoryBase<Project>, IProjectRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProjectRepositoryImpl"/> class.
        /// </summary>
        /// <param name="contextFactory">The context factory.</param>
        public ProjectRepositoryImpl(IContextFactory contextFactory)
            : base(contextFactory)
        {
        }
    }
}
