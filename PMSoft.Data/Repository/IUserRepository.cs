﻿using PMSoft.Data.Infrastructure;
using PMSoft.Models;

namespace PMSoft.Data.Repository
{
    /// <summary>
    /// Repository interface for <see cref="ApplicationUser"/> entity
    /// </summary>
    public interface IUserRepository : IRepository<ApplicationUser>
    {
    }
}
