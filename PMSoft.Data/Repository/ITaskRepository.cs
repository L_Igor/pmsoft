﻿using PMSoft.Data.Infrastructure;
using PMSoft.Models;

namespace PMSoft.Data.Repository
{
    /// <summary>
    /// Repository interface for <see cref="Task"/> entity
    /// </summary>
    public interface ITaskRepository : IRepository<Task>
    {
    }
}
