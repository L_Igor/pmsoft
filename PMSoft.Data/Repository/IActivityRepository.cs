﻿using PMSoft.Data.Infrastructure;
using PMSoft.Models;

namespace PMSoft.Data.Repository
{
    /// <summary>
    /// Repository interface for <see cref="Activity"/> entity
    /// </summary>
    public interface IActivityRepository : IRepository<Activity>
    {
    }
}
