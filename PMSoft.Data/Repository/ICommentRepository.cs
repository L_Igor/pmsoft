﻿using PMSoft.Data.Infrastructure;
using PMSoft.Models;

namespace PMSoft.Data.Repository
{
    /// <summary>
    /// Repository interface for <see cref="Comment"/> entity
    /// </summary>
    public interface ICommentRepository : IRepository<Comment>
    {
    }
}
