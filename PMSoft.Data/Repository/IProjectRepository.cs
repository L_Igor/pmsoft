﻿using PMSoft.Data.Infrastructure;
using PMSoft.Models;

namespace PMSoft.Data.Repository
{
    /// <summary>
    /// Repository interface for <see cref="Project"/> entity
    /// </summary>
    public interface IProjectRepository : IRepository<Project>
    {
    }
}
