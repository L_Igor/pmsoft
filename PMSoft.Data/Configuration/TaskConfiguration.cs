﻿using System.Data.Entity.ModelConfiguration;

using PMSoft.Models;

namespace PMSoft.Data.Configuration
{
    /// <summary>
    /// EntityFramework configuration for <see cref="Task"/> entity
    /// </summary>
    public class TaskConfiguration : EntityTypeConfiguration<Task>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TaskConfiguration"/> class.
        /// </summary>
        public TaskConfiguration()
        {
            ToTable("Tasks");

            HasKey(p => p.Id);

            Property(p => p.Title).IsRequired().HasMaxLength(128);
            Property(p => p.Description).IsOptional().HasMaxLength(1024);
            Property(p => p.CreateDateTimeUtc).IsRequired();
            Property(p => p.UpdateDateTimeUtc).IsOptional();
            Property(p => p.DeleteDateTimeUtc).IsOptional();

            HasRequired(p => p.Project)
                .WithMany(p => p.Tasks)
                .Map(p => p.MapKey("ProjectId"))
                /*.WillCascadeOnDelete(false)*/;
        }
    }
}