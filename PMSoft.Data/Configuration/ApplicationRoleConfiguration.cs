﻿using System.Data.Entity.ModelConfiguration;

using PMSoft.Models;

namespace PMSoft.Data.Configuration
{
    /// <summary>
    /// EntityFramework configuration for <see cref="ApplicationRole"/> entity
    /// </summary>
    class ApplicationRoleConfiguration : EntityTypeConfiguration<ApplicationRole>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ApplicationRoleConfiguration"/> class.
        /// </summary>
        public ApplicationRoleConfiguration()
        {
            //ToTable("Roles");
        }
    }
}
