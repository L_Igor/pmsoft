﻿using System.Data.Entity.ModelConfiguration;

using PMSoft.Models;

namespace PMSoft.Data.Configuration
{
    /// <summary>
    /// EntityFramework configuration for <see cref="Comment"/> entity
    /// </summary>
    public class CommentConfiguration : EntityTypeConfiguration<Comment>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CommentConfiguration"/> class.
        /// </summary>
        public CommentConfiguration()
        {
            ToTable("Comments");

            HasKey(p => p.Id);

            Property(p => p.Text).IsRequired().HasMaxLength(1024);
            Property(p => p.CreateDateTimeUtc).IsRequired();
            Property(p => p.UpdateDateTimeUtc).IsOptional();
            Property(p => p.DeleteDateTimeUtc).IsOptional();

            HasRequired(p => p.Task)
                .WithMany(p => p.Comments)
                .Map(p => p.MapKey("TaskId"))
                /*.WillCascadeOnDelete(false)*/;

            HasRequired(p => p.User)
               .WithMany(p => p.Comments)
               .Map(p => p.MapKey("UserId"))
               /*.WillCascadeOnDelete(false)*/;
        }
    }
}