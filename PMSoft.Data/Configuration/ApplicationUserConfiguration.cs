﻿using System.Data.Entity.ModelConfiguration;

using PMSoft.Models;

namespace PMSoft.Data.Configuration
{
    /// <summary>
    /// EntityFramework configuration for <see cref="ApplicationUser"/> entity
    /// </summary>
    class ApplicationUserConfiguration : EntityTypeConfiguration<ApplicationUser>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ApplicationUserConfiguration"/> class.
        /// </summary>
        public ApplicationUserConfiguration()
        {
            ToTable("Users");

            Property(p => p.FirstName).IsOptional().HasMaxLength(256);
            Property(p => p.LastName).IsOptional().HasMaxLength(256);
            Property(p => p.MiddleName).IsOptional().HasMaxLength(259);
            Property(p => p.CreateDateTimeUtc).IsRequired();
            Property(p => p.UpdateDateTimeUtc).IsOptional();
            Property(p => p.DeleteDateTimeUtc).IsOptional();

            //Ignore(p => p.Email);
            //Ignore(p => p.EmailConfirmed);
            //Ignore(p => p.PhoneNumber);
            //Ignore(p => p.PhoneNumberConfirmed);
            //Ignore(p => p.TwoFactorEnabled);
            //Ignore(p => p.LockoutEndDateUtc);
            //Ignore(p => p.LockoutEnabled);
            //Ignore(p => p.AccessFailedCount);

            HasMany(p => p.Roles)
                .WithRequired()
                .HasForeignKey(p => p.UserId);
        }
    }
}
