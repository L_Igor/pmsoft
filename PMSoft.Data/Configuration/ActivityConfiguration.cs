﻿using System.Data.Entity.ModelConfiguration;

using PMSoft.Models;

namespace PMSoft.Data.Configuration
{
    /// <summary>
    /// EntityFramework configuration for <see cref="Activity"/> entity
    /// </summary>
    public class ActivityConfiguration : EntityTypeConfiguration<Activity>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ActivityConfiguration"/> class.
        /// </summary>
        public ActivityConfiguration()
        {
            ToTable("Activities");

            HasKey(p => p.Id);

            Property(p => p.StartDateTimeUtc).IsRequired();
            Property(p => p.EndDateTimeUtc).IsRequired();
            Property(p => p.Activeness).IsRequired();
            Property(p => p.CreateDateTimeUtc).IsRequired();
            Property(p => p.UpdateDateTimeUtc).IsOptional();
            Property(p => p.DeleteDateTimeUtc).IsOptional();

            HasRequired(p => p.Task)
                .WithMany(p => p.Activities)
                .Map(p => p.MapKey("TaskId"))
                /*.WillCascadeOnDelete(false)*/;

            HasRequired(p => p.User)
                .WithMany(p => p.Activities)
                .Map(p => p.MapKey("UserId"))
                /*.WillCascadeOnDelete(false)*/;
        }
    }
}