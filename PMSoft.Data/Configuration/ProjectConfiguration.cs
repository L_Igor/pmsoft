﻿using System.Data.Entity.ModelConfiguration;

using PMSoft.Models;

namespace PMSoft.Data.Configuration
{
    /// <summary>
    /// EntityFramework configuration for <see cref="Project"/> entity
    /// </summary>
    public class ProjectConfiguration : EntityTypeConfiguration<Project>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProjectConfiguration"/> class.
        /// </summary>
        public ProjectConfiguration()
        {
            ToTable("Projects");

            HasKey(p => p.Id);

            Property(p => p.Title).IsRequired().HasMaxLength(128);
            Property(p => p.CreateDateTimeUtc).IsRequired();
            Property(p => p.UpdateDateTimeUtc).IsOptional();
            Property(p => p.DeleteDateTimeUtc).IsOptional();

            HasMany(p => p.Users)
                .WithMany(p => p.Projects)
                .Map(p =>
                {
                    p.ToTable("UsersProjects");
                    p.MapLeftKey("ProjectId");
                    p.MapRightKey("UserId");
                });
        }
    }
}