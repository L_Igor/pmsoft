﻿using System;

namespace PMSoft.Data.Infrastructure
{
    /// <summary>
    /// The default context factory implementation
    /// </summary>
    class DefaultContextFactoryImpl : Disposable, IContextFactory
    {
        /// <summary>
        /// Performs actual disposing
        /// </summary>
        protected override void DisposeCore()
        {
            if (_dataContext != null)
                _dataContext.Dispose();
        }

        /// <summary>
        /// Gets the current context from the factory.
        /// </summary>
        /// <returns></returns>
        public ApplicationDbContext GetContext()
        {
            if (IsDisposed)
                throw new ObjectDisposedException("ContextFactory");

            return _dataContext ?? (_dataContext = new ApplicationDbContext());
        }

        /// <summary>
        /// The cached instacne of the data context
        /// </summary>
        private ApplicationDbContext _dataContext;
    }
}
