﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace PMSoft.Data.Infrastructure
{
    /// <summary>
    /// Base class for entity repositories 
    /// </summary>
    /// <typeparam name="T">Entity type</typeparam>
    public interface IRepository<T> where T : class
    {
        /// <summary>
        /// Gets the entity by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>Entity from repository</returns>
        T GetById(Int64 id);
        /// <summary>
        /// Adds the specified entity into the repository.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns>
        /// Current repository instance for chained operations
        /// </returns>
        IRepository<T> Add(T entity);
        /// <summary>
        /// Adds the specified entity into the repository.
        /// </summary>
        /// <param name="entities">The collection of entities.</param>
        /// <returns>
        /// Current repository instance for chained operations
        /// </returns>
        IRepository<T> AddRange(IEnumerable<T> entities);
        /// <summary>
        /// Deletes the range.
        /// </summary>
        /// <param name="entities">The entities.</param>
        void DeleteRange(IEnumerable<T> entities);
        /// <summary>
        /// Updates the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns>
        /// Current repository instance for chained operations
        /// </returns>
        IRepository<T> Update(T entity);
        /// <summary>
        /// Deletes the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns>
        /// Current repository instance for chained operations
        /// </returns>
        IRepository<T> Delete(T entity);
        /// <summary>
        /// Queries this repository - provides ability to query using LINQ through entities inside repository.
        /// </summary>
        /// <returns></returns>
        IQueryable<T> Query();
    }
}
