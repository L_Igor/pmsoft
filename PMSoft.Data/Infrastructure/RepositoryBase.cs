﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace PMSoft.Data.Infrastructure
{
    /// <summary>
    /// The repository base
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class RepositoryBase<T> : IRepository<T> where T : class
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RepositoryBase{T}"/> class.
        /// </summary>
        /// <param name="contextFactory">The context factory.</param>
        protected RepositoryBase(IContextFactory contextFactory)
        {
            _contextFactory = contextFactory;
        }

        /// <summary>
        /// Gets the entity by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>Found entity or null</returns>
        public virtual T GetById(Int64 id)
        {
            return ContextFactory.GetContext().Set<T>().Find(id);
        }

        /// <summary>
        /// Adds the specified entity into the repository.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns>
        /// Current repository instance for chained operations
        /// </returns>
        public virtual IRepository<T> Add(T entity)
        {
            ContextFactory.GetContext().Set<T>().Add(entity);
            return this;
        }

        /// <summary>
        /// Adds the specified entity into the repository.
        /// </summary>
        /// <param name="entities">The collection of entities.</param>
        /// <returns>
        /// Current repository instance for chained operations
        /// </returns>
        public IRepository<T> AddRange(IEnumerable<T> entities)
        {
            ContextFactory.GetContext().Set<T>().AddRange(entities);
            return this;
        }

        /// <summary>
        /// Deletes the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns>
        /// Current repository instance for chained operations
        /// </returns>
        public virtual IRepository<T> Delete(T entity)
        {
            ContextFactory.GetContext().Set<T>().Remove(entity);
            return this;
        }

        public void DeleteRange(IEnumerable<T> entities)
        {
            ContextFactory.GetContext().Set<T>().RemoveRange(entities);
        }

        /// <summary>
        /// Updates the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns>
        /// Current repository instance for chained operations
        /// </returns>
        public virtual IRepository<T> Update(T entity)
        {
            ContextFactory.GetContext().Set<T>().Attach(entity);
            ContextFactory.GetContext().Entry(entity).State = EntityState.Modified;
            return this;
        }

        /// <summary>
        /// Queries this repository - provides ability to query using LINQ through entities inside repository.
        /// </summary>
        public virtual IQueryable<T> Query()
        {
            return ContextFactory.GetContext().Set<T>();
        }

        protected IContextFactory ContextFactory
        {
            get { return _contextFactory; }
        }

        /// <summary>
        /// The transaction
        /// </summary>
        private DbContextTransaction _transaction;

        /// <summary>
        /// Current instance of the context factory
        /// </summary>
        private readonly IContextFactory _contextFactory;
    }
}
