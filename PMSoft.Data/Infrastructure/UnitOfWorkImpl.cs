﻿namespace PMSoft.Data.Infrastructure
{
    public class UnitOfWorkImpl : IUnitOfWork
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UnitOfWorkImpl"/> class.
        /// </summary>
        /// <param name="contextFactory">The context factory.</param>
        public UnitOfWorkImpl(IContextFactory contextFactory)
        {
            _contextFactory = contextFactory;
        }

        /// <summary>
        /// Gets the data context.
        /// </summary>
        protected ApplicationDbContext DataContext
        {
            get { return _dataContext ?? (_dataContext = _contextFactory.GetContext()); }
        }

        /// <summary>
        /// Commits current changes in the unit of work
        /// </summary>
        public void Commit()
        {
            DataContext.SaveChanges();
        }

        /// <summary>
        /// The context factory
        /// </summary>
        private readonly IContextFactory _contextFactory;
        /// <summary>
        /// The data context
        /// </summary>
        private ApplicationDbContext _dataContext;
    }
}
