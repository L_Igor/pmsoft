﻿namespace PMSoft.Data.Infrastructure
{
    public interface IUnitOfWork
    {
        /// <summary>
        /// Commits current changes in the unit of work
        /// </summary>
        void Commit();
    }
}
