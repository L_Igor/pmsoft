﻿using System;

namespace PMSoft.Data.Infrastructure
{
    /// <summary>
    /// Provides basic interface for data context factory
    /// </summary>
    public interface IContextFactory : IDisposable
    {
        /// <summary>
        /// Gets the current context from the factory.
        /// </summary>
        /// <returns></returns>
        ApplicationDbContext GetContext();
    }
}
