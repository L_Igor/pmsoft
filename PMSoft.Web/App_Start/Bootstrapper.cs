﻿using Microsoft.AspNet.Identity;
using PMSoft.Models;
using PMSoft.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PMSoft.Web.App_Start
{
    /// <summary>
    /// Contains application startup and setup tasks
    /// </summary>
    public static class Bootstrapper
    {
        /// <summary>
        /// Runs startup tasks. Should be called from Global.asax
        /// </summary>
        public static void Run()
        {
            CreateRoles();
            CreateCreatorUser();
        }

        private static void CreateRoles()
        {
            var roles = new[]
            {
                 new ApplicationRole
                {
                    Name = UserRoleType.Creator.ToString()
                },
                new ApplicationRole
                {
                    Name = UserRoleType.Admin.ToString()
                },
                new ApplicationRole
                {
                    Name = UserRoleType.Member.ToString()
                }
            };

            foreach (var role in roles)
            {
                var roleManager = DependencyResolver.Current.GetService<ApplicationRoleManager>();

                if (!roleManager.RoleExists(role.Name))
                {
                    roleManager.Create(role);
                }
            }
        }

        private static void CreateCreatorUser()
        {
            var creatorUser = new ApplicationUser
            {
                UserName = "creator@mail.com",
                Email = "creator@mail.com",
                EmailConfirmed = true,
                CreateDateTimeUtc = DateTime.UtcNow,
            };

            var userManager = DependencyResolver.Current.GetService<ApplicationUserManager>();
            if (userManager.FindByName(creatorUser.UserName) == null)
            {
                var result = userManager.Create(creatorUser, creatorUser.UserName);
                if (result.Succeeded)
                {
                    userManager.AddToRoles(creatorUser.Id,
                        UserRoleType.Creator.ToString(),
                        UserRoleType.Admin.ToString(),
                        UserRoleType.Member.ToString());
                }
                else
                {
                    throw new Exception("Can't create creator user. Something is wrong: " + String.Join(", ", result.Errors));
                }
            }
        }
    }
}