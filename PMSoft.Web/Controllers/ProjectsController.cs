﻿using Microsoft.AspNet.Identity;
using PMSoft.Services.Interfaces;
using PMSoft.Services.Models.BindingModels;
using PMSoft.Services.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace PMSoft.Web.Controllers
{
    [Authorize]
    [RoutePrefix("api/Projects")]
    public class ProjectsController : BaseApiController
    {
        public ProjectsController(IProjectService projectService
            , ITaskService taskService
            , IAdminService adminService
            , IUserService userService)
        {
            _projectService = projectService;
            _taskService = taskService;
            _adminService = adminService;
            _userService = userService;
        }

        #region public methods

        // GET: api/Projects
        [HttpGet]
        public IHttpActionResult GetAllProjects()
        {
            var requesterUserId = User.Identity.GetUserId();
            var result = _projectService.GetAllProjects(requesterUserId);

            if (result.Errors.Any())
            {
                UpdateModelState(result);
                return BadRequest(ModelState);
            }

            return Ok(result.Data);
        }

        // GET: api/Projects/5
        [HttpGet]
        [Route("{projectId}")]
        [ResponseType(typeof(ProjectViewModel))]
        public IHttpActionResult GetProject(Int32 projectId)
        {
            var requesterUserId = User.Identity.GetUserId();
            var result = _projectService.GetProjectById(projectId, requesterUserId);

            if (result.Errors.Any())
            {
                UpdateModelState(result);
                return BadRequest(ModelState);
            }

            return Ok(result.Data);
        }

        // POST: api/Projects
        [HttpPost]
        [ResponseType(typeof(ProjectViewModel))]
        public IHttpActionResult CreateProject(CreateProjectBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var requesterUserId = User.Identity.GetUserId();
            var result = _projectService.CreateProject(model, requesterUserId);

            if (result.Errors.Any())
            {
                UpdateModelState(result);
                return BadRequest(ModelState);
            }

            return CreatedAtRoute("DefaultApi", new { result.Data.Id }, result.Data);
        }

        // PUT: api/Projects/5
        [HttpPut]
        [Route("{projectId}")]
        [ResponseType(typeof(ProjectViewModel))]
        public IHttpActionResult EditProject(Int32 projectId, UpdateProjectBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (projectId != model.Id)
            {
                return BadRequest();
            }

            var requesterUserId = User.Identity.GetUserId();
            var result = _projectService.UpdateProject(model, requesterUserId);

            if (result.Errors.Any())
            {
                UpdateModelState(result);
                return BadRequest(ModelState);
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // DELETE: api/Projects/5
        [HttpDelete]
        [Route("{projectId}")]
        public IHttpActionResult DeleteProject(Int32 projectId)
        {
            var requesterUserId = User.Identity.GetUserId();
            var result = _projectService.DeleteProject(projectId, requesterUserId);

            if (result.Errors.Any())
            {
                UpdateModelState(result);
                return BadRequest(ModelState);
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // GET: api/Projects/5/Tasks
        [HttpGet]
        [Route("{projectId}/Tasks")]
        [ResponseType(typeof(TaskViewModel))]
        public IHttpActionResult GetProjectTasks(Int32 projectId)
        {
            var requesterUserId = User.Identity.GetUserId();
            var result = _taskService.GetAllProjectTasks(projectId, requesterUserId);

            if (result.Errors.Any())
            {
                UpdateModelState(result);
                return BadRequest(ModelState);
            }

            return Ok(result.Data);
        }

        // GET: api/Projects/5/Users
        [HttpGet]
        [Route("{projectId}/Users")]
        [ResponseType(typeof(TaskViewModel))]
        public IHttpActionResult GetProjectUsers(Int32 projectId)
        {
            var requesterUserId = User.Identity.GetUserId();
            var result = _userService.GetAllProjectUsers(projectId, requesterUserId);

            if (result.Errors.Any())
            {
                UpdateModelState(result);
                return BadRequest(ModelState);
            }

            return Ok(result.Data);
        }

        // PUT: api/Projects/5/Users/f8a3f258-bfe6-4dca-a682-c31e9e42f49c
        [HttpPut]
        [Route("{projectId}/Users/{userId}")]
        [ResponseType(typeof(ProjectViewModel))]
        public IHttpActionResult AssignProjectToUser(Int32 projectId, String userId)
        {
            var requesterUserId = User.Identity.GetUserId();
            var result = _adminService.AssignProjectToUser(projectId, userId, requesterUserId);

            if (result.Errors.Any())
            {
                UpdateModelState(result);
                return BadRequest(ModelState);
            }

            return Ok(result.Data);
        }

        // DELETE: api/Projects/5/Users/f8a3f258-bfe6-4dca-a682-c31e9e42f49c
        [HttpDelete]
        [Route("{projectId}/Users/{userId}")]
        public IHttpActionResult DeAssignProjectToUser(Int32 projectId, String userId)
        {
            var requesterUserId = User.Identity.GetUserId();
            var result = _adminService.DeAssignProjectToUser(projectId, userId, requesterUserId);

            if (result.Errors.Any())
            {
                UpdateModelState(result);
                return BadRequest(ModelState);
            }

            return Ok(result.Data);
        }

        #endregion

        #region private fields

        private readonly IProjectService _projectService;
        private readonly ITaskService _taskService;
        private readonly IAdminService _adminService;
        private readonly IUserService _userService;

        #endregion
    }
}
