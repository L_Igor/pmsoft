﻿using Microsoft.AspNet.Identity;
using PMSoft.Services.Interfaces;
using PMSoft.Services.Models.BindingModels;
using PMSoft.Services.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace PMSoft.Web.Controllers
{
    [Authorize]
    [RoutePrefix("api/Comments")]
    public class CommentsController : BaseApiController
    {
        public CommentsController(ICommentService commentService)
        {
            _commentService = commentService;
        }

        #region public methods

        // GET: api/Comments
        [HttpGet]
        public IHttpActionResult GetAllComments()
        {
            var requesterUserId = User.Identity.GetUserId();
            var result = _commentService.GetAllComments(requesterUserId);

            if (result.Errors.Any())
            {
                UpdateModelState(result);
                return BadRequest(ModelState);
            }

            return Ok(result.Data);
        }

        // GET: api/Comments/5
        [HttpGet]
        [Route("{commentId}")]
        [ResponseType(typeof(CommentViewModel))]
        public IHttpActionResult GetComment(Int32 commentId)
        {
            var requesterUserId = User.Identity.GetUserId();
            var result = _commentService.GetCommentById(commentId, requesterUserId);

            if (result.Errors.Any())
            {
                UpdateModelState(result);
                return BadRequest(ModelState);
            }

            return Ok(result.Data);
        }

        // POST: api/Comments
        [HttpPost]
        [ResponseType(typeof(CommentViewModel))]
        public IHttpActionResult CreateComment(CreateCommentBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var requesterUserId = User.Identity.GetUserId();
            var result = _commentService.CreateComment(model, requesterUserId);

            if (result.Errors.Any())
            {
                UpdateModelState(result);
                return BadRequest(ModelState);
            }

            return CreatedAtRoute("DefaultApi", new { result.Data.Id }, result.Data);
        }

        // PUT: api/Comments/5
        [HttpPut]
        [Route("{commentId}")]
        [ResponseType(typeof(CommentViewModel))]
        public IHttpActionResult EditComment(Int32 commentId, UpdateCommentBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (commentId != model.Id)
            {
                return BadRequest();
            }

            var requesterUserId = User.Identity.GetUserId();
            var result = _commentService.UpdateComment(model, requesterUserId);

            if (result.Errors.Any())
            {
                UpdateModelState(result);
                return BadRequest(ModelState);
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // DELETE: api/Comments/5
        [HttpDelete]
        [Route("{commentId}")]
        public IHttpActionResult DeleteComment(Int32 commentId)
        {
            var requesterUserId = User.Identity.GetUserId();
            var result = _commentService.DeleteComment(commentId, requesterUserId);

            if (result.Errors.Any())
            {
                UpdateModelState(result);
                return BadRequest(ModelState);
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        #endregion

        #region private methods


        #endregion

        #region private fields

        private readonly ICommentService _commentService;

        #endregion
    }
}
