﻿using Microsoft.AspNet.Identity;
using PMSoft.Services.Interfaces;
using PMSoft.Services.Models.BindingModels;
using PMSoft.Services.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace PMSoft.Web.Controllers
{
    [Authorize]
    [RoutePrefix("api/Activities")]
    public class ActivitiesController : BaseApiController
    {
        public ActivitiesController(IActivityService activityService)
        {
            _activityService = activityService;
        }

        #region public methods

        // GET: api/Activities
        [HttpGet]
        public IHttpActionResult GetAllActivities()
        {
            var requesterUserId = User.Identity.GetUserId();
            var result = _activityService.GetAllActivities(requesterUserId);

            if (result.Errors.Any())
            {
                UpdateModelState(result);
                return BadRequest(ModelState);
            }

            return Ok(result.Data);
        }

        // GET: api/Activities/5
        [HttpGet]
        [Route("{activityId}")]
        [ResponseType(typeof(ActivityViewModel))]
        public IHttpActionResult GetActivity(Int32 activityId)
        {
            var requesterUserId = User.Identity.GetUserId();
            var result = _activityService.GetActivityById(activityId, requesterUserId);

            if (result.Errors.Any())
            {
                UpdateModelState(result);
                return BadRequest(ModelState);
            }

            return Ok(result.Data);
        }

        // POST: api/Activities
        [HttpPost]
        [ResponseType(typeof(ActivityViewModel))]
        public IHttpActionResult CreateActivity(CreateActivityBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var requesterUserId = User.Identity.GetUserId();
            var result = _activityService.CreateActivity(model, requesterUserId);

            if (result.Errors.Any())
            {
                UpdateModelState(result);
                return BadRequest(ModelState);
            }

            return CreatedAtRoute("DefaultApi", new { result.Data.Id }, result.Data);
        }

        // PUT: api/Activities/5
        [HttpPut]
        [Route("{activityId}")]
        [ResponseType(typeof(ActivityViewModel))]
        public IHttpActionResult EditActivity(Int32 activityId, UpdateActivityBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (activityId != model.Id)
            {
                return BadRequest();
            }

            var requesterUserId = User.Identity.GetUserId();
            var result = _activityService.UpdateActivity(model, requesterUserId);

            if (result.Errors.Any())
            {
                UpdateModelState(result);
                return BadRequest(ModelState);
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // DELETE: api/Activities/5
        [HttpDelete]
        [Route("{activityId}")]
        public IHttpActionResult DeleteActivity(Int32 activityId)
        {
            var requesterUserId = User.Identity.GetUserId();
            var result = _activityService.DeleteActivity(activityId, requesterUserId);

            if (result.Errors.Any())
            {
                UpdateModelState(result);
                return BadRequest(ModelState);
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        #endregion

        #region private methods


        #endregion

        #region private fields

        private readonly IActivityService _activityService;

        #endregion
    }
}
