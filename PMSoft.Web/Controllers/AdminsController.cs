﻿using Microsoft.AspNet.Identity;
using PMSoft.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace PMSoft.Web.Controllers
{
    [Authorize]
    [RoutePrefix("api/Admins")]
    public class AdminsController : BaseApiController
    {
        public AdminsController(ICreatorService creatorService
            , IUserService userService)
        {
            _creatorService = creatorService;
            _userService = userService;
        }

        #region public methods

        // GET: api/Admins
        [HttpGet]
        public IHttpActionResult GetAllAdmins()
        {
            var requesterUserId = User.Identity.GetUserId();
            var result = _userService.GetAllAdmins(requesterUserId);

            if (result.Errors.Any())
            {
                UpdateModelState(result);
                return BadRequest(ModelState);
            }

            return Ok(result.Data);
        }

        // PUT: api/Admins/{adminId}
        [HttpPut]
        [Route("{adminId}")]
        [ResponseType(typeof(Boolean))]
        public IHttpActionResult GrantAdminPermission(String adminId)
        {
            var requesterUserId = User.Identity.GetUserId();
            var result = _creatorService.GrantAdminPermission(adminId, requesterUserId);

            if (result.Errors.Any())
            {
                UpdateModelState(result);
                return BadRequest(ModelState);
            }

            return Ok(result.Data);
        }

        // DELETE: api/Admins/{adminId}
        [HttpDelete]
        [Route("{adminId}")]
        [ResponseType(typeof(Boolean))]
        public IHttpActionResult RevokeAdminPermission(String adminId)
        {
            var requesterUserId = User.Identity.GetUserId();
            var result = _creatorService.RevokeAdminPermission(adminId, requesterUserId);

            if (result.Errors.Any())
            {
                UpdateModelState(result);
                return BadRequest(ModelState);
            }

            return Ok(result.Data);
        }

        #endregion

        #region private methods


        #endregion

        #region private fields

        private readonly ICreatorService _creatorService;
        private readonly IUserService _userService;

        #endregion
    }
}
