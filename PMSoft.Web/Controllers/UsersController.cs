﻿using Microsoft.AspNet.Identity;
using PMSoft.Models;
using PMSoft.Services;
using PMSoft.Services.Interfaces;
using PMSoft.Services.Models.BindingModels;
using PMSoft.Services.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace PMSoft.Web.Controllers
{
    [Authorize]
    [RoutePrefix("api/Users")]
    public class UsersController : BaseApiController
    {
        public UsersController(IUserService userService
            , IProjectService projectService
            , ITaskService taskService
            , IActivityService activityService)
        {
            _userService = userService;
            _projectService = projectService;
            _taskService = taskService;
            _activityService = activityService;
        }

        #region public methods

        // GET: api/Users
        [HttpGet]
        public IHttpActionResult GetAllUsers()
        {
            var requesterUserId = User.Identity.GetUserId();
            var result = _userService.GetAllUsers(requesterUserId);

            if (result.Errors.Any())
            {
                UpdateModelState(result);
                return BadRequest(ModelState);
            }

            return Ok(result.Data);
        }

        // GET: api/Users/{userId}
        [HttpGet]
        [Route("{userId:guid}")]
        [ResponseType(typeof(UserViewModel))]
        public IHttpActionResult GetUser(String userId)
        {
            var requesterUserId = User.Identity.GetUserId();
            var result = _userService.GetUserById(userId, requesterUserId);

            if (result.Errors.Any())
            {
                return NotFound();
            }

            return Ok(result.Data);
        }

        // POST: api/Users
        [HttpPost]
        [ResponseType(typeof(UserViewModel))]
        public IHttpActionResult CreateUser(CreateUserBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var requesterUserId = User.Identity.GetUserId();
            var result = _userService.CreateUser(model, requesterUserId);

            if (result.Errors.Any())
            {
                UpdateModelState(result);
                return BadRequest(ModelState);
            }

            return Ok(result.Data);
        }

        // PUT: api/Users/{userId}
        [HttpPut]
        [Route("{userId:guid}")]
        [ResponseType(typeof(void))]
        public IHttpActionResult UpdateUserInfo(String userId, [FromBody]UpdateUserInfoBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (userId != model.Id)
            {
                return BadRequest();
            }

            var requesterUserId = User.Identity.GetUserId();
            var result = _userService.UpdateUserInfo(model, requesterUserId);

            if (result.Errors.Any())
            {
                UpdateModelState(result);
                return BadRequest(ModelState);
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // GET: api/Users/{userId}/Projects
        [HttpGet]
        [Route("{userId:guid}/Projects")]
        [ResponseType(typeof(ProjectViewModel))]
        public IHttpActionResult GetUserProjects(String userId)
        {
            var requesterUserId = User.Identity.GetUserId();
            var result = _projectService.GetAllUserProjects(userId, requesterUserId);

            if (result.Errors.Any())
            {
                UpdateModelState(result);
                return BadRequest(ModelState);
            }

            return Ok(result.Data);
        }

        // GET: api/Users/{userId}/Tasks
        [HttpGet]
        [Route("{userId:guid}/Tasks")]
        [ResponseType(typeof(ProjectViewModel))]
        public IHttpActionResult GetUserTasks(String userId)
        {
            var requesterUserId = User.Identity.GetUserId();
            var result = _taskService.GetAllUserTasks(userId, requesterUserId);

            if (result.Errors.Any())
            {
                UpdateModelState(result);
                return BadRequest(ModelState);
            }

            return Ok(result.Data);
        }

        // GET: api/Users/{userId}/Activities
        [HttpGet]
        [Route("{userId:guid}/Activities")]
        [ResponseType(typeof(ActivityViewModel))]
        public IHttpActionResult GetUserActivities(String userId)
        {
            var requesterUserId = User.Identity.GetUserId();
            var result = _activityService.GetAllUserActivities(userId, requesterUserId);

            if (result.Errors.Any())
            {
                UpdateModelState(result);
                return BadRequest(ModelState);
            }

            return Ok(result.Data);
        }

        #endregion

        #region private methods


        #endregion

        #region private fields

        private readonly IUserService _userService;
        private readonly IProjectService _projectService;
        private readonly ITaskService _taskService;
        private readonly IActivityService _activityService;

        #endregion

    }
}
