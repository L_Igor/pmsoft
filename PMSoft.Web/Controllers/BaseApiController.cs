﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PMSoft.Web.Controllers
{
    public class BaseApiController : ApiController
    {
        protected void UpdateModelState<T>(Services.Models.Result<T> result)
        {
            if (result.Errors.Any())
            {
                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError(String.Empty, error);
                }
            }
        }
    }
}
