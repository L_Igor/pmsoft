﻿using Microsoft.AspNet.Identity;
using PMSoft.Services.Interfaces;
using PMSoft.Services.Models.BindingModels;
using PMSoft.Services.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace PMSoft.Web.Controllers
{
    [Authorize]
    [RoutePrefix("api/Tasks")]
    public class TasksController : BaseApiController
    {
        public TasksController(ITaskService taskService
            , ICommentService commentService
            , IActivityService activityService)
        {
            _taskService = taskService;
            _commentService = commentService;
            _activityService = activityService;
        }

        #region public methods

        // GET: api/Tasks
        [HttpGet]
        public IHttpActionResult GetAllTasks()
        {
            var requesterUserId = User.Identity.GetUserId();
            var result = _taskService.GetAllTasks(requesterUserId);

            if (result.Errors.Any())
            {
                UpdateModelState(result);
                return BadRequest(ModelState);
            }

            return Ok(result.Data);
        }

        // GET: api/Tasks/5
        [HttpGet]
        [Route("{taskId}")]
        [ResponseType(typeof(TaskViewModel))]
        public IHttpActionResult GetTask(Int32 taskId)
        {
            var requesterUserId = User.Identity.GetUserId();
            var result = _taskService.GetTaskById(taskId, requesterUserId);

            if (result.Errors.Any())
            {
                UpdateModelState(result);
                return BadRequest(ModelState);
            }

            return Ok(result.Data);
        }

        // POST: api/Tasks
        [HttpPost]
        [ResponseType(typeof(TaskViewModel))]
        public IHttpActionResult CreateTask(CreateTaskBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var requesterUserId = User.Identity.GetUserId();
            var result = _taskService.CreateTask(model, requesterUserId);

            if (result.Errors.Any())
            {
                UpdateModelState(result);
                return BadRequest(ModelState);
            }

            return CreatedAtRoute("DefaultApi", new { result.Data.Id }, result.Data);
        }

        // PUT: api/Tasks/5
        [HttpPut]
        [Route("{taskId}")]
        [ResponseType(typeof(TaskViewModel))]
        public IHttpActionResult EditTask(Int32 taskId, UpdateTaskBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (taskId != model.Id)
            {
                return BadRequest();
            }

            var requesterUserId = User.Identity.GetUserId();
            var result = _taskService.UpdateTask(model, requesterUserId);

            if (result.Errors.Any())
            {
                UpdateModelState(result);
                return BadRequest(ModelState);
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // DELETE: api/Tasks/5
        [HttpDelete]
        [Route("{taskId}")]
        public IHttpActionResult DeleteTask(Int32 taskId)
        {
            var requesterUserId = User.Identity.GetUserId();
            var result = _taskService.DeleteTask(taskId, requesterUserId);

            if (result.Errors.Any())
            {
                UpdateModelState(result);
                return BadRequest(ModelState);
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // GET: api/Tasks/5/Comments
        [HttpGet]
        [Route("{taskId}/Comments")]
        [ResponseType(typeof(CommentViewModel))]
        public IHttpActionResult GetTaskComments(Int32 taskId)
        {
            var requesterUserId = User.Identity.GetUserId();
            var result = _commentService.GetAllTaskComments(taskId, requesterUserId);

            if (result.Errors.Any())
            {
                UpdateModelState(result);
                return BadRequest(ModelState);
            }

            return Ok(result.Data);
        }

        // GET: api/Tasks/5/Activities
        [HttpGet]
        [Route("{taskId}/Activities")]
        [ResponseType(typeof(ActivityViewModel))]
        public IHttpActionResult GetTaskActivities(Int32 taskId)
        {
            var requesterUserId = User.Identity.GetUserId();
            var result = _activityService.GetAllTaskActivities(taskId, requesterUserId);

            if (result.Errors.Any())
            {
                UpdateModelState(result);
                return BadRequest(ModelState);
            }

            return Ok(result.Data);
        }

        #endregion

        #region private methods


        #endregion

        #region private fields

        private readonly ITaskService _taskService;
        private readonly ICommentService _commentService;
        private readonly IActivityService _activityService;

        #endregion
    }
}
