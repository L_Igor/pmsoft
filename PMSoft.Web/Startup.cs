﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Owin;
using Owin;
using Autofac;
using System.Web.Http;
using Autofac.Integration.WebApi;
using Autofac.Integration.Mvc;
using System.Web.Mvc;
using System.Reflection;
using PMSoft.Data;
using System.Web;
using Microsoft.Owin.Security.DataProtection;
using Microsoft.AspNet.Identity;
using PMSoft.Models;
using PMSoft.Services;
using PMSoft.Web.App_Start;
using PMSoft.Services.Interfaces;

[assembly: OwinStartup(typeof(PMSoft.Web.Startup))]

namespace PMSoft.Web
{
    public partial class Startup
    {
        /// <summary>
        /// Configurations the specified application.
        /// </summary>
        /// <param name="app">The application.</param>
        public void Configuration(IAppBuilder app)
        {
            var container = SetupAutofacContainer(app);
            ConfigureAuth(app, container);
            Bootstrapper.Run();
        }

        /// <summary>
        /// Setup of the Autofac container resolvers.
        /// </summary>
        private static IContainer SetupAutofacContainer(IAppBuilder app)
        {
            var container = BuildContainer(app);

            GlobalConfiguration.Configuration.DependencyResolver = new AutofacWebApiDependencyResolver(container);

            // Get your HttpConfiguration. In OWIN, you'll create one rather than using GlobalConfiguration.
            var config = new HttpConfiguration { DependencyResolver = new AutofacWebApiDependencyResolver(container) };

            // OWIN WEB API SETUP:

            // Register the Autofac middleware FIRST, then the Autofac Web API middleware, and finally the standard Web API middleware.
            app.UseAutofacMiddleware(container);
            app.UseAutofacWebApi(config);
            app.UseWebApi(config);

            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
            return container;
        }

        /// <summary>
        /// Builds the Autofac container.
        /// </summary>
        /// <param name="app">The application builder object.</param>
        /// <returns>Built container</returns>
        private static IContainer BuildContainer(IAppBuilder app)
        {
            var builder = new ContainerBuilder();

            builder.RegisterControllers(Assembly.GetExecutingAssembly());
            // Register your Web API controllers.
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());

            builder.RegisterAssemblyTypes(typeof(ApplicationDbContext).Assembly)
               .Where(t => t.Name.EndsWith("Impl"))
               .AsImplementedInterfaces()
               .InstancePerRequest();

            builder.RegisterType(typeof(ApplicationUserStore))
                .As<IUserStore<ApplicationUser>>()
                .InstancePerRequest();
            builder.RegisterType(typeof(ApplicationUserManager)).AsSelf().InstancePerRequest();

            builder.RegisterType(typeof(ApplicationRoleStore))
                .As<IRoleStore<ApplicationRole, String>>()
                .InstancePerRequest();
            builder.RegisterType(typeof(ApplicationRoleManager)).AsSelf().InstancePerRequest();

            builder.Register(c => HttpContext.Current.GetOwinContext().Authentication).InstancePerRequest();
            builder.Register(c => app.GetDataProtectionProvider()).InstancePerRequest();


            #region register services

            builder.RegisterAssemblyTypes(typeof(ICreatorService).Assembly)
               .Where(t => t.Name.EndsWith("Impl"))
               .AsImplementedInterfaces()
               .InstancePerRequest();

            builder.RegisterAssemblyTypes(typeof(IAdminService).Assembly)
               .Where(t => t.Name.EndsWith("Impl"))
               .AsImplementedInterfaces()
               .InstancePerRequest();

            builder.RegisterAssemblyTypes(typeof(IUserService).Assembly)
               .Where(t => t.Name.EndsWith("Impl"))
               .AsImplementedInterfaces()
               .InstancePerRequest();

            builder.RegisterAssemblyTypes(typeof(IProjectService).Assembly)
               .Where(t => t.Name.EndsWith("Impl"))
               .AsImplementedInterfaces()
               .InstancePerRequest();

            builder.RegisterAssemblyTypes(typeof(ITaskService).Assembly)
               .Where(t => t.Name.EndsWith("Impl"))
               .AsImplementedInterfaces()
               .InstancePerRequest();

            builder.RegisterAssemblyTypes(typeof(ICommentService).Assembly)
               .Where(t => t.Name.EndsWith("Impl"))
               .AsImplementedInterfaces()
               .InstancePerRequest();

            #endregion

            return builder.Build();
        }
    }
}