﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PMSoft.Models
{
    public class Activity
    {
        public Int32 Id { get; set; }
        public DateTime StartDateTimeUtc { get; set; }
        public DateTime EndDateTimeUtc { get; set; }
        public Double Activeness { get; set; }
        public DateTime CreateDateTimeUtc { get; set; }
        public DateTime? UpdateDateTimeUtc { get; set; }
        public DateTime? DeleteDateTimeUtc { get; set; }

        public virtual Task Task { get; set; }
        public virtual ApplicationUser User { get; set; }
    }
}
