﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PMSoft.Models
{
    public class Project
    {
        public Int32 Id { get; set; }
        public String Title { get; set; }
        public DateTime CreateDateTimeUtc { get; set; }
        public DateTime? UpdateDateTimeUtc { get; set; }
        public DateTime? DeleteDateTimeUtc { get; set; }

        public virtual ICollection<ApplicationUser> Users { get; set; } // many to many relationship
        public virtual ICollection<Task> Tasks { get; set; }
    }
}