﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PMSoft.Models
{
    public class Task
    {
        public Int32 Id { get; set; }
        public String Title { get; set; }
        public String Description { get; set; }
        public DateTime CreateDateTimeUtc { get; set; }
        public DateTime? UpdateDateTimeUtc { get; set; }
        public DateTime? DeleteDateTimeUtc { get; set; }

        public virtual Project Project { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }
        public virtual ICollection<Activity> Activities { get; set; }
    }
}