namespace PMSoft.Data.Migrations.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedApplicationRoleModel : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.AspNetUserRoles", "ApplicationUser_Id", "dbo.Users");
            DropIndex("dbo.AspNetUserRoles", new[] { "ApplicationUser_Id" });
            //DropPrimaryKey("dbo.AspNetUserRoles");
            DropColumn("dbo.AspNetUserRoles", "ApplicationUser_Id");
            //AddPrimaryKey("dbo.AspNetUserRoles", new[] { "UserId", "RoleId" });
            CreateIndex("dbo.AspNetUserRoles", "UserId");
            AddForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.Users", "Id");
            AddColumn("dbo.AspNetRoles", "Discriminator", c => c.String(nullable: false, maxLength: 128));

            //DropIndex("dbo.AspNetUserRoles", new[] { "ApplicationUser_Id" });
            //DropColumn("dbo.AspNetUserRoles", "UserId");
            //RenameColumn(table: "dbo.AspNetUserRoles", name: "ApplicationUser_Id", newName: "UserId");
            //DropPrimaryKey("dbo.AspNetUserRoles");
            //AddColumn("dbo.AspNetRoles", "Discriminator", c => c.String(nullable: false, maxLength: 128));
            //AlterColumn("dbo.AspNetUserRoles", "UserId", c => c.String(nullable: false, maxLength: 128));
            //AddPrimaryKey("dbo.AspNetUserRoles", new[] { "UserId", "RoleId" });
            //CreateIndex("dbo.AspNetUserRoles", "UserId");
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetRoles", "Discriminator");
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.Users");
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            //DropPrimaryKey("dbo.AspNetUserRoles");
            AddColumn("dbo.AspNetUserRoles", "ApplicationUser_Id", c => c.String(nullable: false, maxLength: 128));
            //AddPrimaryKey("dbo.AspNetUserRoles", new[] { "UserId", "RoleId" });
            CreateIndex("dbo.AspNetUserRoles", "ApplicationUser_Id");
            AddForeignKey("dbo.AspNetUserRoles", "ApplicationUser_Id", "dbo.Users", "Id");

            //DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            //DropPrimaryKey("dbo.AspNetUserRoles");
            //AlterColumn("dbo.AspNetUserRoles", "UserId", c => c.String(maxLength: 128));
            //DropColumn("dbo.AspNetRoles", "Discriminator");
            //AddPrimaryKey("dbo.AspNetUserRoles", new[] { "UserId", "RoleId" });
            //RenameColumn(table: "dbo.AspNetUserRoles", name: "UserId", newName: "ApplicationUser_Id");
            //AddColumn("dbo.AspNetUserRoles", "UserId", c => c.String(nullable: false, maxLength: 128));
            //CreateIndex("dbo.AspNetUserRoles", "ApplicationUser_Id");
        }
    }
}
