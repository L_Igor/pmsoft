namespace PMSoft.Data.Migrations.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MovedDescribeRelationship : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.UsersProjects");
            AddPrimaryKey("dbo.UsersProjects", new[] { "ProjectId", "UserId" });
        }
        
        public override void Down()
        {
            DropPrimaryKey("dbo.UsersProjects");
            AddPrimaryKey("dbo.UsersProjects", new[] { "UserId", "ProjectId" });
        }
    }
}
