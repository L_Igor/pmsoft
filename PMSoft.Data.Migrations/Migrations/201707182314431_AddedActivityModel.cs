namespace PMSoft.Data.Migrations.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedActivityModel : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Activities",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        StartDateTimeUtc = c.DateTime(nullable: false),
                        EndDateTimeUtc = c.DateTime(nullable: false),
                        Activeness = c.Double(nullable: false),
                        CreateDateTimeUtc = c.DateTime(nullable: false),
                        UpdateDateTimeUtc = c.DateTime(),
                        DeleteDateTimeUtc = c.DateTime(),
                        TaskId = c.Int(nullable: false),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Tasks", t => t.TaskId, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.TaskId)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Activities", "UserId", "dbo.Users");
            DropForeignKey("dbo.Activities", "TaskId", "dbo.Tasks");
            DropIndex("dbo.Activities", new[] { "UserId" });
            DropIndex("dbo.Activities", new[] { "TaskId" });
            DropTable("dbo.Activities");
        }
    }
}
