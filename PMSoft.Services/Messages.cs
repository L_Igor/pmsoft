﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PMSoft.Services
{
    public static class Messages
    {
        public static readonly String AccessDenied = "Доступ запрещен";
        public static readonly String ActionForbidden = "Действие запрещено";
        public static readonly String RemovedUser = "Пользователь был удален";
        public static readonly String RemovedProject = "Проект был удален";
        public static readonly String RemovedTask = "Задача была удалена";
        public static readonly String RemovedComment = "Комментарий был удален";
        public static readonly String RemovedActivity = "Активность была удалена";
        public static readonly String EmptyProjectTitle = "Не задано название проекта";
        public static readonly String EmptyTaskTitle = "Не задано название задачи";
        public static readonly String ActivityRange = "Значение активности должно находиться в диапазоне от 0 до 1";
        public static readonly String StartDateTimeLessEndDateTime = "Дата начала активности должна быть меньше времени конца";
        public static readonly String StartDateTimeAndEndDateTimeLessDateTimeNow = "Даты начала и конца активности должны быть меньше текущего времени";
        public static readonly String ValidStartDateTime = "Нельзя создать активность начатую раньше суток назад";
        public static readonly String ValidDurationOfActivity = "Нельзя создать активность длительностью более получаса";
        public static readonly String NotFoundUser = "Указанный пользователь не существует";
        public static readonly String NotFoundProject = "Указанный проект не существует";
        public static readonly String NotFoundTask = "Указанная задача не существует";
        public static readonly String NotFoundComment = "Указанный комментарий не существует";
        public static readonly String NotFoundActivity = "Указанная активность не существует";
    }
}
