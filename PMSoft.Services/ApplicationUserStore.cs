﻿using Microsoft.AspNet.Identity.EntityFramework;
using PMSoft.Data;
using PMSoft.Data.Infrastructure;
using PMSoft.Models;

namespace PMSoft.Services
{
    public class ApplicationUserStore : UserStore<ApplicationUser>
    {
        public ApplicationUserStore(IContextFactory contextFactory)
            : base(contextFactory.GetContext())
        {
        }
    }
}