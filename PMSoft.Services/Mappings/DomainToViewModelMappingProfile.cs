﻿using System;

using AutoMapper;

using PMSoft.Models;
using PMSoft.Services.Models.ViewModels;

namespace Blog.Mappings
{
    /// <summary>
    /// Contains mapping configuration for AutoMapper that should convert Domain Model entities to View Model classes.
    /// </summary>
    public class DomainToViewModelMappingProfile : Profile
    {
        public DomainToViewModelMappingProfile()
            : base("DomainToViewModelMappings")
        {
            // todo: mapping to add here

            #region mapping from ApplicationUser to *ViewModel

            CreateMap<ApplicationUser, UserViewModel>();

            CreateMap<Project, ProjectViewModel>();

            CreateMap<Task, TaskViewModel>();

            CreateMap<Comment, CommentViewModel>();

            CreateMap<Project, ProjectViewModel>();

            CreateMap<Activity, ActivityViewModel>();

            #endregion
        }
    }
}