﻿using System;

using AutoMapper;

using PMSoft.Models;
using PMSoft.Services.Models.BindingModels;

namespace Blog.Mappings
{
    /// <summary>
    /// Contains mapping configuration for AutoMapper that should convert View Model classes to Domain Model entities.
    /// </summary>
    public class BindingModelToDomainMappingProfile : Profile
    {
        public BindingModelToDomainMappingProfile()
            : base("BindingModelToDomainMappingProfile")
        {
            // todo: mapping to add here

            #region mapping from *ViewModel to ApplicationUser

            CreateMap<CreateUserBindingModel, ApplicationUser>()
                .ForMember(dst => dst.UserName, opt => opt.MapFrom(src => src.Email));
            CreateMap<UpdateUserInfoBindingModel, ApplicationUser>();

            CreateMap<CreateProjectBindingModel, Project>();
            CreateMap<UpdateProjectBindingModel, Project>();

            CreateMap<CreateTaskBindingModel, Task>();
            CreateMap<UpdateTaskBindingModel, Task>();

            CreateMap<CreateCommentBindingModel, Comment>();
            CreateMap<UpdateCommentBindingModel, Comment>();

            CreateMap<CreateActivityBindingModel, Activity>();
            CreateMap<UpdateActivityBindingModel, Activity>();

            #endregion
        }
    }
}