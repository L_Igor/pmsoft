﻿using AutoMapper;

namespace Blog.Mappings
{
    /// <summary>
    /// Contains configuration routines for AutoMapper
    /// </summary>
    public class AutoMapperConfiguration
    {
        /// <summary>
        /// Configures AutoMapper. Should be called on application start.
        /// </summary>
        public static void Configure()
        {
            Mapper.Initialize(x =>
            {
                x.AddProfile<DomainToViewModelMappingProfile>();
                x.AddProfile<BindingModelToDomainMappingProfile>();
                x.AddProfile<ModelToViewModelMappingProfile>();
            });
        }
    }
}