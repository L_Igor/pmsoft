﻿using System;

using AutoMapper;

namespace Blog.Mappings
{
    /// <summary>
    /// Contains mapping configuration for AutoMapper that should convert Model classes to View models.
    /// </summary>
    public class ModelToViewModelMappingProfile : Profile
    {
        public ModelToViewModelMappingProfile()
            : base("ModelToViewModelMappings")
        {
            // todo: mapping to add here
        }
    }
}