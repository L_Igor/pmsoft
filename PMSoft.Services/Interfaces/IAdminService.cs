﻿using PMSoft.Models;
using PMSoft.Services.Models;
using PMSoft.Services.Models.BindingModels;
using PMSoft.Services.Models.ViewModels;
using System;
using System.Collections.Generic;

namespace PMSoft.Services.Interfaces
{
    public interface IAdminService
    {
        Result<Boolean> AssignProjectToUser(Int32 projectId, String userId, String requesterUserId);
        Result<Boolean> DeAssignProjectToUser(Int32 projectId, String userId, String requesterUserId);
    }
}
