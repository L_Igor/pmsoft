﻿using PMSoft.Services.Models;
using PMSoft.Services.Models.BindingModels;
using PMSoft.Services.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PMSoft.Services.Interfaces
{
    public interface ITaskService
    {
        Result<TaskViewModel> GetTaskById(Int32 taskId, String requesterUserId);
        Result<IEnumerable<TaskViewModel>> GetAllTasks(String requesterUserId);
        Result<IEnumerable<TaskViewModel>> GetAllProjectTasks(Int32 projectId, String requesterUserId);
        Result<IEnumerable<TaskViewModel>> GetAllUserTasks(String userId, String requesterUserId);
        Result<TaskViewModel> CreateTask(CreateTaskBindingModel taskModel, String requesterUserId);
        Result<Boolean> UpdateTask(UpdateTaskBindingModel taskModel, String requesterUserId);
        Result<Boolean> DeleteTask(Int32 taskId, String requesterUserId);
    }
}
