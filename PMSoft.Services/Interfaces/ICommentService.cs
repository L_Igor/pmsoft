﻿using PMSoft.Services.Models;
using PMSoft.Services.Models.BindingModels;
using PMSoft.Services.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PMSoft.Services.Interfaces
{
    public interface ICommentService
    {
        Result<CommentViewModel> GetCommentById(Int32 commentId, String requesterUserId);
        Result<IEnumerable<CommentViewModel>> GetAllComments(String requesterUserId);
        Result<IEnumerable<CommentViewModel>> GetAllTaskComments(Int32 taskId, String requesterUserId);
        Result<CommentViewModel> CreateComment(CreateCommentBindingModel commentModel, String requesterUserId);
        Result<Boolean> UpdateComment(UpdateCommentBindingModel commentModel, String requesterUserId);
        Result<Boolean> DeleteComment(Int32 commentId, String requesterUserId);
    }
}
