﻿using PMSoft.Models;
using PMSoft.Services.Models;
using PMSoft.Services.Models.ViewModels;
using System;
using System.Collections.Generic;

namespace PMSoft.Services.Interfaces
{
    public interface ICreatorService
    {
        Result<Boolean> GrantAdminPermission(String userId, String requesterUserId);
        Result<Boolean> RevokeAdminPermission(String userId, String requesterUserId);
    }
}
