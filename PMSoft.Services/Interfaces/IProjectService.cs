﻿using PMSoft.Services.Models;
using PMSoft.Services.Models.BindingModels;
using PMSoft.Services.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PMSoft.Services.Interfaces
{
    public interface IProjectService
    {
        Result<ProjectViewModel> GetProjectById(Int32 projectId, String requesterUserId);
        Result<IEnumerable<ProjectViewModel>> GetAllProjects(String requesterUserId);
        Result<IEnumerable<ProjectViewModel>> GetAllUserProjects(String userId, String requesterUserId);
        Result<ProjectViewModel> CreateProject(CreateProjectBindingModel projectModel, String requesterUserId);
        Result<Boolean> UpdateProject(UpdateProjectBindingModel projectModel, String requesterUserId);
        Result<Boolean> DeleteProject(Int32 projectId, String requesterUserId);
    }
}
