﻿using PMSoft.Models;
using PMSoft.Services.Models;
using PMSoft.Services.Models.BindingModels;
using PMSoft.Services.Models.ViewModels;
using System;
using System.Collections.Generic;

namespace PMSoft.Services.Interfaces
{
    public interface IUserService
    {
        Result<UserViewModel> GetUserById(String userId, String requesterUserId);
        Result<IEnumerable<UserViewModel>> GetAllUsers(String requesterUserId);
        Result<IEnumerable<UserViewModel>> GetAllAdmins(String requesterUserId);
        Result<IEnumerable<UserViewModel>> GetAllProjectUsers(Int32 projectId, String requesterUserId);
        Result<UserViewModel> CreateUser(CreateUserBindingModel userModel, String requesterUserId);
        Result<Boolean> UpdateUserInfo(UpdateUserInfoBindingModel userModel, String requesterUserId);
        Boolean IsAdmin(String userId);
        Boolean IsCreator(String userId);
    }
}
