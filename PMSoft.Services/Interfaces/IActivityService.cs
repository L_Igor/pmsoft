﻿using PMSoft.Services.Models;
using PMSoft.Services.Models.BindingModels;
using PMSoft.Services.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PMSoft.Services.Interfaces
{
    public interface IActivityService
    {
        Result<ActivityViewModel> GetActivityById(Int32 activityId, String requesterUserId);
        Result<IEnumerable<ActivityViewModel>> GetAllActivities(String requesterUserId);
        Result<IEnumerable<ActivityViewModel>> GetAllTaskActivities(Int32 taskId, String requesterUserId);
        Result<IEnumerable<ActivityViewModel>> GetAllUserActivities(String userId, String requesterUserId);
        Result<ActivityViewModel> CreateActivity(CreateActivityBindingModel activityModel, String requesterUserId);
        Result<Boolean> UpdateActivity(UpdateActivityBindingModel activityModel, String requesterUserId);
        Result<Boolean> DeleteActivity(Int32 activityId, String requesterUserId);
    }
}
