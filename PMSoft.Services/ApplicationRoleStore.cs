﻿using Microsoft.AspNet.Identity.EntityFramework;
using PMSoft.Data.Infrastructure;
using PMSoft.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PMSoft.Services
{
    public class ApplicationRoleStore : RoleStore<ApplicationRole>
    {
        public ApplicationRoleStore(IContextFactory contextFactory)
            : base(contextFactory.GetContext())
        {
        }
    }
}
