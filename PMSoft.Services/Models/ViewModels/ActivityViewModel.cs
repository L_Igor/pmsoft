﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PMSoft.Services.Models.ViewModels
{
    public class ActivityViewModel
    {
        public Int32 Id { get; set; }
        public DateTime StartDateTimeUtc { get; set; }
        public DateTime EndDateTimeUtc { get; set; }
        public Double Activeness { get; set; }
        public Int32 TaskId { get; set; }
        public String UserId { get; set; }


        //public virtual Task Task { get; set; }
        //public virtual ApplicationUser User { get; set; }
    }
}
