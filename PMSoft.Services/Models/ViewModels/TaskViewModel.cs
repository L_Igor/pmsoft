﻿using System;
using System.Collections.Generic;

namespace PMSoft.Services.Models.ViewModels
{
    public class TaskViewModel
    {
        public Int32 Id { get; set; }
        public String Title { get; set; }
        public String Description { get; set; }
        public Int32 ProjectId { get; set; }

        //public virtual ProjectViewModel Project { get; set; }
        //public virtual ICollection<CommentViewModel> Comments { get; set; }
    }
}