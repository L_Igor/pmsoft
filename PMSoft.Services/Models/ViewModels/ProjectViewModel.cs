﻿using System;
using System.Collections.Generic;

namespace PMSoft.Services.Models.ViewModels
{
    public class ProjectViewModel
    {
        public Int32 Id { get; set; }
        public String Title { get; set; }

        //public virtual ICollection<TaskViewModel> Tasks { get; set; }
        //public virtual ICollection<UserViewModel> Users { get; set; }
    }
}