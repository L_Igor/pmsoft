﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PMSoft.Services.Models.ViewModels
{
    public class ApplicationRoleViewModel
    {
        public String Id { get; set; }
        public String Name { get; set; }
    }
}
