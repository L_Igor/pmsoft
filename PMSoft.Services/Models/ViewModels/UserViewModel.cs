﻿using System;
using System.Collections.Generic;

namespace PMSoft.Services.Models.ViewModels
{
    public class UserViewModel
    {
        public String Id { get; set; }
        public String Email { get; set; }
        public String FirstName { get; set; }
        public String LastName { get; set; }
        public String MiddleName { get; set; }

        //public virtual ICollection<ProjectViewModel> Projects { get; set; }
        //public virtual ICollection<CommentViewModel> Comments { get; set; }
    }
}