﻿using System;
using System.Collections.Generic;

namespace PMSoft.Services.Models.ViewModels
{
    public class CommentViewModel
    {
        public Int32 Id { get; set; }
        public String Text { get; set; }
        public Int32 TaskId { get; set; }
        public String UserId { get; set; }

        //public virtual TaskViewModel Task { get; set; }
        //public virtual UserViewModel User { get; set; }
    }
}