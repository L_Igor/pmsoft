﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PMSoft.Services.Models.BindingModels
{
    public class CreateUserBindingModel
    {
        [Required]
        [Display(Name = "Адрес электронной почты")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "Значение {0} должно содержать не менее {2} символов.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Пароль")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Подтверждение пароля")]
        [Compare("Password", ErrorMessage = "Пароль и его подтверждение не совпадают.")]
        public string ConfirmPassword { get; set; }

        [MaxLength(256, ErrorMessage = "Значение {0} должно содержать не более {1} символов.")]
        [Display(Name = "Имя")]
        public String FirstName { get; set; }

        [MaxLength(256, ErrorMessage = "Значение {0} должно содержать не более {1} символов.")]
        [Display(Name = "Фамилия")]
        public String LastName { get; set; }

        [MaxLength(259, ErrorMessage = "Значение {0} должно содержать не более {1} символов.")]
        [Display(Name = "Отчество")]
        public String MiddleName { get; set; }
    }
}