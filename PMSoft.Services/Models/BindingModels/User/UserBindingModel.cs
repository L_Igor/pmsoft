﻿using System;
using System.Collections.Generic;

namespace PMSoft.Services.Models.BindingModels
{
    public class UserBindingModel
    {
        public String Id { get; set; }
        public String Email { get; set; }
        public String Password { get; set; }
        public String FirstName { get; set; }
        public String LastName { get; set; }
        public String MiddleName { get; set; }

        public virtual ICollection<ProjectBindingModel> Projects { get; set; }
        public virtual ICollection<CommentBindingModel> Comments { get; set; }
    }
}