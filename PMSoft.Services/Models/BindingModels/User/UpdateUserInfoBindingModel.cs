﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PMSoft.Services.Models.BindingModels
{
    public class UpdateUserInfoBindingModel
    {
        [Required]
        public String Id { get; set; }

        [MaxLength(256, ErrorMessage = "Значение {0} должно содержать не более {1} символов.")]
        [Display(Name = "Имя")]
        public String FirstName { get; set; }

        [MaxLength(256, ErrorMessage = "Значение {0} должно содержать не более {1} символов.")]
        [Display(Name = "Фамилия")]
        public String LastName { get; set; }

        [MaxLength(259, ErrorMessage = "Значение {0} должно содержать не более {1} символов.")]
        [Display(Name = "Отчество")]
        public String MiddleName { get; set; }
    }
}