﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PMSoft.Services.Models.BindingModels
{
    public class ApplicationRoleBindingModel
    {
        public String Id { get; set; }
        public String Name { get; set; }
    }
}
