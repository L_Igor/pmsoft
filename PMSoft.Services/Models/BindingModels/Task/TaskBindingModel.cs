﻿using System;
using System.Collections.Generic;

namespace PMSoft.Services.Models.BindingModels
{
    public class TaskBindingModel
    {
        public Int32 Id { get; set; }
        public String Title { get; set; }
        public String Description { get; set; }

        public virtual ProjectBindingModel Project { get; set; }
        public virtual ICollection<CommentBindingModel> Comments { get; set; }
    }
}