﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PMSoft.Services.Models.BindingModels
{
    public class CreateTaskBindingModel
    {
        [Required]
        [MinLength(3, ErrorMessage = "Значение {0} должно содержать не менее {1} символов.")]
        [MaxLength(128, ErrorMessage = "Значение {0} должно содержать не более {1} символов.")]
        public String Title { get; set; }

        [MaxLength(1024, ErrorMessage = "Значение {0} должно содержать не более {1} символов.")]
        public String Description { get; set; }

        [Required]
        public Int32 ProjectId { get; set; }
        
        //public virtual ProjectBindingModel Project { get; set; }
        //public virtual ICollection<CommentBindingModel> Comments { get; set; }
    }
}