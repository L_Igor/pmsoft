﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PMSoft.Services.Models.BindingModels
{
    public class UpdateCommentBindingModel
    {
        [Required]
        public Int32 Id { get; set; }

        [Required]
        [MinLength(3, ErrorMessage = "Значение {0} должно содержать не менее {1} символов.")]
        [MaxLength(1024, ErrorMessage = "Значение {0} должно содержать не более {1} символов.")]
        public String Text { get; set; }

        //public virtual TaskBindingModel Task { get; set; }
        //public virtual UserBindingModel User { get; set; }
    }
}