﻿using System;
using System.Collections.Generic;

namespace PMSoft.Services.Models.BindingModels
{
    public class CommentBindingModel
    {
        public Int32 Id { get; set; }
        public String Text { get; set; }

        public virtual TaskBindingModel Task { get; set; }
        public virtual UserBindingModel User { get; set; }
    }
}