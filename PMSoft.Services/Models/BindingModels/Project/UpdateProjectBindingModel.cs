﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PMSoft.Services.Models.BindingModels
{
    public class UpdateProjectBindingModel
    {
        [Required]
        public Int32 Id { get; set; }

        [Required]
        [MinLength(3, ErrorMessage = "Значение {0} должно содержать не менее {1} символов.")]
        [MaxLength(128, ErrorMessage = "Значение {0} должно содержать не более {1} символов.")]
        public String Title { get; set; }

        //public virtual ICollection<TaskBindingModel> Tasks { get; set; }
        //public virtual ICollection<UserBindingModel> Users { get; set; }
    }
}