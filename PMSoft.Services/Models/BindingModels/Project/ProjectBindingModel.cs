﻿using System;
using System.Collections.Generic;

namespace PMSoft.Services.Models.BindingModels
{
    public class ProjectBindingModel
    {
        public Int32 Id { get; set; }
        public String Title { get; set; }

        public virtual ICollection<TaskBindingModel> Tasks { get; set; }
        public virtual ICollection<UserBindingModel> Users { get; set; }
    }
}