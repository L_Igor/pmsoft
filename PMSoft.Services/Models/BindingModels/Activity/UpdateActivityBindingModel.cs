﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PMSoft.Services.Models.BindingModels
{
    public class UpdateActivityBindingModel
    {
        [Required]
        public Int32 Id { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        public DateTime StartDateTimeUtc { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        public DateTime EndDateTimeUtc { get; set; }

        [Required]
        [Range(0.0, 1.0)]
        public Double Activeness { get; set; }

        //public virtual Task Task { get; set; }
        //public virtual ApplicationUser User { get; set; }
    }
}
