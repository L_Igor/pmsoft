﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PMSoft.Services.Models.BindingModels
{
    public class ActivityBindingModel
    {
        public Int32 Id { get; set; }
        public DateTime StartDateTimeUtc { get; set; }
        public DateTime EndDateTimeUtc { get; set; }
        public Double Activeness { get; set; }

        public virtual TaskBindingModel Task { get; set; }
        public virtual UserBindingModel User { get; set; }
    }
}
