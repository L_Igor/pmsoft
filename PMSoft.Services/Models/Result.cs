﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PMSoft.Services.Models
{
    public class Result<T>
    {
        public Result()
        {
            Errors = new List<String>();
        }

        public T Data { get; set; }
        public IEnumerable<String> Errors { get; set; }
    }
}
