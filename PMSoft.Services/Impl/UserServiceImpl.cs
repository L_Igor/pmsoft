﻿using PMSoft.Data.Infrastructure;
using PMSoft.Data.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using PMSoft.Models;
using PMSoft.Services.Models;
using System.Data.Entity;
using PMSoft.Services.Models.ViewModels;
using AutoMapper;
using PMSoft.Services.Models.BindingModels;
using PMSoft.Services.Interfaces;

namespace PMSoft.Services.Impl
{
    public class UserServiceImpl : IUserService
    {
        public UserServiceImpl(IUnitOfWork unitOfWork
            , IUserRepository userRepository
            , IProjectRepository projectRepository
            , ApplicationUserManager userManager
            , ApplicationRoleManager roleManager)
        {
            _unitOfWork = unitOfWork;
            _userRepository = userRepository;
            _projectRepository = projectRepository;
            _userManager = userManager;
            _roleManager = roleManager;
        }

        #region public methods

        public Result<UserViewModel> GetUserById(String userId, String requesterUserId)
        {
            var result = new Result<UserViewModel>();

            var user = _userManager.FindByIdAsync(userId).Result;
            if (user == null)
            {
                result.Errors = new List<String> { Messages.NotFoundUser };
                return result;
            }
            if (user.DeleteDateTimeUtc != null)
            {
                result.Errors = new List<String> { Messages.RemovedUser };
                return result;
            }
            //var creatorId =
            //     _roleManager.FindByNameAsync(UserRoleType.Creator.ToString()).Result
            //         .Users.First().UserId;

            var hasNotAccess =
                _userManager.IsInRoleAsync(userId, UserRoleType.Creator.ToString()).Result &&
                !(user.Id == requesterUserId /*&& requesterUser.Id == creatorId*/);

            if (hasNotAccess)
            {
                result.Errors = new List<String> { Messages.AccessDenied };
            }
            else
            {
                result.Data = Mapper.Map<UserViewModel>(user);
            }

            return result;
        }

        public Result<IEnumerable<UserViewModel>> GetAllUsers(String requesterUserId)
        {
            var result = new Result<IEnumerable<UserViewModel>>();

            if (!IsAdmin(requesterUserId))
            {
                result.Errors = new List<String> { Messages.AccessDenied };
            }
            else
            {
                var users = _GetUsersByRole(UserRoleType.Member);
                result.Data = Mapper.Map<IEnumerable<UserViewModel>>(users);
            }

            return result;
        }

        public Result<IEnumerable<UserViewModel>> GetAllAdmins(String requesterUserId)
        {
            var result = new Result<IEnumerable<UserViewModel>>();

            if (!IsCreator(requesterUserId))
            {
                result.Errors = new List<String> { Messages.AccessDenied };
            }
            else
            {
                var users = _GetUsersByRole(UserRoleType.Admin);
                result.Data = Mapper.Map<IEnumerable<UserViewModel>>(users);
            }

            return result;
        }

        public Result<IEnumerable<UserViewModel>> GetAllProjectUsers(Int32 projectId, String requesterUserId)
        {
            var result = new Result<IEnumerable<UserViewModel>>();

            var project = _projectRepository.GetById(projectId);
            if (project == null)
            {
                result.Errors = new List<String> { Messages.NotFoundProject };
                return result;
            }
            if (project.DeleteDateTimeUtc != null)
            {
                result.Errors = new List<String> { Messages.RemovedProject };
                return result;
            }

            var isAdminRequesterUser = IsAdmin(requesterUserId);
            var hasAccess = project.Users.Any(u => u.Id == requesterUserId) || isAdminRequesterUser;

            if (hasAccess)
            {
                var tasks =
                    _userRepository.Query()
                        .Where(p => p.DeleteDateTimeUtc == null &&
                                    p.Projects.Any(x => x.DeleteDateTimeUtc == null && x.Id == projectId));
                result.Data = Mapper.Map<IEnumerable<UserViewModel>>(tasks);
            }
            else
            {
                result.Errors = new List<String> { Messages.AccessDenied };
            }

            return result;
        }

        public Result<UserViewModel> CreateUser(CreateUserBindingModel userModel, String requesterUserId)
        {
            var result = new Result<UserViewModel>();

            if (!IsAdmin(requesterUserId))
            {
                result.Errors = new List<String> { Messages.AccessDenied };
                return result;
            }

            var user = Mapper.Map<ApplicationUser>(userModel);
            user.CreateDateTimeUtc = DateTime.UtcNow;

            var identityResult = _userManager.CreateAsync(user, userModel.Password).Result;
            if (!identityResult.Succeeded)
            {
                result.Errors = identityResult.Errors;
                return result;
            }

            identityResult = _userManager.AddToRoleAsync(user.Id, UserRoleType.Member.ToString()).Result;
            if (!identityResult.Succeeded)
            {
                result.Errors = identityResult.Errors;
                return result;
            }

            result.Data = Mapper.Map<UserViewModel>(user);

            return result;
        }

        public Result<Boolean> UpdateUserInfo(UpdateUserInfoBindingModel userModel, String requesterUserId)
        {
            var result = new Result<Boolean>() { Data = false };

            if (requesterUserId != userModel.Id)
            {
                result.Errors = new List<String> { Messages.AccessDenied };
                return result;
            }

            var user = _userManager.FindByIdAsync(userModel.Id).Result;
            if (user == null)
            {
                result.Errors = new List<String> { Messages.NotFoundUser };
                return result;
            }
            if (user.DeleteDateTimeUtc != null)
            {
                result.Errors = new List<String> { Messages.RemovedUser };
                return result;
            }

            user.FirstName = userModel.FirstName;
            user.LastName = userModel.LastName;
            user.MiddleName = userModel.MiddleName;
            user.UpdateDateTimeUtc = DateTime.UtcNow;
            var identityResult = _userManager.UpdateAsync(user).Result;
            if (!identityResult.Succeeded)
            {
                result.Errors = identityResult.Errors;
                return result;
            }

            result.Data = true;

            return result;
        }

        public Boolean IsAdmin(String userId)
        {
            return
                _userManager.IsInRoleAsync(userId, UserRoleType.Admin.ToString()).Result;
        }

        public Boolean IsCreator(String userId)
        {
            return
               _userManager.IsInRoleAsync(userId, UserRoleType.Creator.ToString()).Result;
        }

        #endregion

        #region private methods

        private IEnumerable<ApplicationUser> _GetUsersByRole(UserRoleType userRoleType)
        {
            var roleId = _roleManager.FindByNameAsync(userRoleType.ToString()).Result.Id;
            var creatorId =
                _roleManager.FindByNameAsync(UserRoleType.Creator.ToString()).Result
                    .Users.First().UserId;

            return
                _userManager.Users
                    .Where(u =>
                        u.Roles.Any(r => r.UserId != creatorId &&
                                         r.RoleId.Equals(roleId, StringComparison.InvariantCultureIgnoreCase)));
        }

        #endregion

        #region private fields

        private readonly IUnitOfWork _unitOfWork;
        private readonly IUserRepository _userRepository;
        private readonly IProjectRepository _projectRepository;
        private readonly ApplicationUserManager _userManager;
        private readonly ApplicationRoleManager _roleManager;

        #endregion
    }
}
