﻿using PMSoft.Data.Infrastructure;
using PMSoft.Data.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using PMSoft.Models;
using PMSoft.Services.Models;
using System.Data.Entity;
using PMSoft.Services.Models.BindingModels;
using PMSoft.Services.Models.ViewModels;
using AutoMapper;
using PMSoft.Services.Interfaces;

namespace PMSoft.Services.Impl
{
    public class AdminServiceImpl : IAdminService
    {
        public AdminServiceImpl(IUnitOfWork unitOfWork
            , IUserRepository userRepository
            , IProjectRepository projectRepository
            , ApplicationUserManager userManager
            , ApplicationRoleManager roleManager
            , IUserService userService)
        {
            _unitOfWork = unitOfWork;
            _projectRepository = projectRepository;
            _userManager = userManager;
            _roleManager = roleManager;
            _userService = userService;
        }

        #region public methods

        public Result<Boolean> AssignProjectToUser(Int32 projectId, String userId, String requesterUserId)
        {
            var result = new Result<Boolean>() { Data = false };

            if (!_userService.IsAdmin(requesterUserId))
            {
                result.Errors = new List<String> { Messages.AccessDenied };
                return result;
            }

            var project = _projectRepository.GetById(projectId);
            if (project == null)
            {
                result.Errors = new List<String> { Messages.NotFoundProject };
                return result;
            }
            if (project.DeleteDateTimeUtc != null)
            {
                result.Errors = new List<String> { Messages.RemovedProject };
                return result;
            }

            var user = _userManager.FindByIdAsync(userId).Result;
            if (user == null)
            {
                result.Errors = new List<String> { Messages.NotFoundUser };
                return result;
            }
            if (user.DeleteDateTimeUtc != null)
            {
                result.Errors = new List<String> { Messages.RemovedUser };
                return result;
            }

            project.Users.Add(user);
            _projectRepository.Update(project);
            _unitOfWork.Commit();

            result.Data = true;

            return result;
        }

        public Result<Boolean> DeAssignProjectToUser(Int32 projectId, String userId, String requesterUserId)
        {
            var result = new Result<Boolean>() { Data = false };

            if (!_userService.IsAdmin(requesterUserId))
            {
                result.Errors = new List<String> { Messages.AccessDenied };
                return result;
            }

            var project = _projectRepository.GetById(projectId);
            if (project == null)
            {
                result.Errors = new List<String> { Messages.NotFoundProject };
                return result;
            }
            if (project.DeleteDateTimeUtc != null)
            {
                result.Errors = new List<String> { Messages.RemovedProject };
                return result;
            }

            var user = _userManager.FindByIdAsync(userId).Result;
            if (user == null)
            {
                result.Errors = new List<String> { Messages.NotFoundUser };
                return result;
            }
            if (user.DeleteDateTimeUtc != null)
            {
                result.Errors = new List<String> { Messages.RemovedUser };
                return result;
            }

            project.Users.Remove(user);
            _projectRepository.Update(project);
            _unitOfWork.Commit();

            result.Data = true;

            return result;
        }

        #endregion

        #region private fields

        private readonly IUnitOfWork _unitOfWork;
        private readonly IProjectRepository _projectRepository;
        private readonly ApplicationUserManager _userManager;
        private readonly ApplicationRoleManager _roleManager;
        private readonly IUserService _userService;

        #endregion
    }
}
