﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PMSoft.Services.Interfaces;
using PMSoft.Services.Models;
using PMSoft.Services.Models.BindingModels;
using PMSoft.Services.Models.ViewModels;
using PMSoft.Data.Infrastructure;
using PMSoft.Data.Repository;
using AutoMapper;
using PMSoft.Models;

using Task = PMSoft.Models.Task;

namespace PMSoft.Services.Impl
{
    public class CommentServiceImpl : ICommentService
    {
        public CommentServiceImpl(IUnitOfWork unitOfWork
            , ICommentRepository commentRepository
            , ITaskRepository taskRepository
            , ApplicationUserManager userManager
            , IUserService userService)
        {
            _unitOfWork = unitOfWork;
            _commentRepository = commentRepository;
            _taskRepository = taskRepository;
            _userManager = userManager;
            _userService = userService;
        }

        #region public methods

        public Result<CommentViewModel> GetCommentById(Int32 commentId, String requesterUserId)
        {
            var result = new Result<CommentViewModel>();

            var comment = _commentRepository.GetById(commentId);
            if (comment == null)
            {
                result.Errors = new List<String> { Messages.NotFoundComment };
                return result;
            }
            if (comment.DeleteDateTimeUtc != null)
            {
                result.Errors = new List<String> { Messages.RemovedComment };
                return result;
            }

            if (_HasAccess(comment.Task, requesterUserId))
            {
                result.Data = Mapper.Map<CommentViewModel>(comment);
            }
            else
            {
                result.Errors = new List<String> { Messages.AccessDenied };
            }

            return result;
        }

        public Result<IEnumerable<CommentViewModel>> GetAllComments(String requesterUserId)
        {
            var result = new Result<IEnumerable<CommentViewModel>>();

            var hasAccess = _userService.IsAdmin(requesterUserId);

            if (hasAccess)
            {
                var tasks = _commentRepository.Query().Where(p => p.DeleteDateTimeUtc == null);
                result.Data = Mapper.Map<IEnumerable<CommentViewModel>>(tasks);
            }
            else
            {
                result.Errors = new List<String> { Messages.AccessDenied };
            }

            return result;
        }

        public Result<IEnumerable<CommentViewModel>> GetAllTaskComments(Int32 taskId, String requesterUserId)
        {
            var result = new Result<IEnumerable<CommentViewModel>>();

            var task = _taskRepository.GetById(taskId);
            if (task == null)
            {
                result.Errors = new List<String> { Messages.NotFoundTask };
                return result;
            }
            if (task.DeleteDateTimeUtc != null)
            {
                result.Errors = new List<String> { Messages.RemovedTask };
                return result;
            }

            var isAdminRequesterUser = _userService.IsAdmin(requesterUserId);
            if (_HasAccess(task, requesterUserId))
            {
                var comments =
                    _commentRepository.Query()
                        .Where(p => p.DeleteDateTimeUtc == null &&
                                    p.Task.Id == taskId &&
                                    (p.Task.Project.Users.Any(u => u.Id == requesterUserId) || isAdminRequesterUser));
                result.Data = Mapper.Map<IEnumerable<CommentViewModel>>(comments);
            }
            else
            {
                result.Errors = new List<String> { Messages.AccessDenied };
            }

            return result;
        }

        public Result<CommentViewModel> CreateComment(CreateCommentBindingModel commentModel, String requesterUserId)
        {
            var result = new Result<CommentViewModel>();

            var user = _userManager.FindByIdAsync(commentModel.UserId).Result;
            if (user == null)
            {
                result.Errors = new List<String> { Messages.NotFoundUser };
                return result;
            }
            if (user.DeleteDateTimeUtc != null)
            {
                result.Errors = new List<String> { Messages.RemovedUser };
                return result;
            }

            if (user.Id != requesterUserId)
            {
                result.Errors = new List<String> { Messages.ActionForbidden };
                return result;
            }

            var task = _taskRepository.GetById(commentModel.TaskId);
            if (task == null)
            {
                result.Errors = new List<String> { Messages.NotFoundTask };
                return result;
            }
            if (task.DeleteDateTimeUtc != null)
            {
                result.Errors = new List<String> { Messages.RemovedTask };
                return result;
            }

            if (!_HasAccess(task, requesterUserId))
            {
                result.Errors = new List<String> { Messages.AccessDenied };
                return result;
            }

            if (String.IsNullOrWhiteSpace(commentModel.Text))
            {
                result.Errors = new List<String> { Messages.EmptyTaskTitle };
                return result;
            }

            var comment = Mapper.Map<Comment>(commentModel);
            comment.Task = task;
            comment.User = user;
            comment.CreateDateTimeUtc = DateTime.UtcNow;
            _commentRepository.Add(comment);

            _unitOfWork.Commit();

            result.Data = Mapper.Map<CommentViewModel>(comment);

            return result;
        }

        public Result<Boolean> UpdateComment(UpdateCommentBindingModel commentModel, String requesterUserId)
        {
            var result = new Result<Boolean>() { Data = false };

            var comment = _commentRepository.GetById(commentModel.Id);
            if (comment == null)
            {
                result.Errors = new List<String> { Messages.NotFoundComment };
                return result;
            }
            if (comment.DeleteDateTimeUtc != null)
            {
                result.Errors = new List<String> { Messages.RemovedComment };
                return result;
            }

            if (!_HasAccess(comment.Task, requesterUserId))
            {
                result.Errors = new List<String> { Messages.AccessDenied };
                return result;
            }

            comment.Text = commentModel.Text;
            comment.UpdateDateTimeUtc = DateTime.UtcNow;
            _commentRepository.Update(comment);

            _unitOfWork.Commit();

            result.Data = true;

            return result;
        }

        public Result<Boolean> DeleteComment(Int32 commentId, String requesterUserId)
        {
            var result = new Result<Boolean>() { Data = false };

            var comment = _commentRepository.GetById(commentId);
            if (comment == null)
            {
                result.Errors = new List<String> { Messages.NotFoundComment };
                return result;
            }
            if (comment.DeleteDateTimeUtc != null)
            {
                result.Errors = new List<String> { Messages.RemovedComment };
                return result;
            }

            if (!_HasAccess(comment.Task, requesterUserId))
            {
                result.Errors = new List<String> { Messages.AccessDenied };
                return result;
            }

            comment.DeleteDateTimeUtc = DateTime.UtcNow;
            _commentRepository.Update(comment);

            _unitOfWork.Commit();

            result.Data = true;

            return result;
        }

        #endregion

        #region private methods

        private Boolean _HasAccess(Task task, String requesterUserId)
        {
            var requesterUser = _userManager.FindByIdAsync(requesterUserId).Result;
            //var task = _taskRepository.GetById(taskId);

            var hasAccess =
                (
                    requesterUser != null &&
                    task != null && task.DeleteDateTimeUtc == null &&
                    task.Project != null && task.Project.DeleteDateTimeUtc == null &&
                    task.Project.Users.Any(u => u.Id == requesterUser.Id)
                ) ||
                _userService.IsAdmin(requesterUserId);

            return hasAccess;
        }

        #endregion

        #region private fields

        private readonly IUnitOfWork _unitOfWork;
        private readonly ICommentRepository _commentRepository;
        private readonly ITaskRepository _taskRepository;
        private readonly ApplicationUserManager _userManager;
        private readonly IUserService _userService;

        #endregion
    }
}
