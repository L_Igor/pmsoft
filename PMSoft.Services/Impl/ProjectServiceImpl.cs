﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PMSoft.Services.Interfaces;
using PMSoft.Services.Models;
using PMSoft.Services.Models.BindingModels;
using PMSoft.Services.Models.ViewModels;
using PMSoft.Data.Infrastructure;
using PMSoft.Data.Repository;
using AutoMapper;
using PMSoft.Models;

namespace PMSoft.Services.Impl
{
    class ProjectServiceImpl : IProjectService
    {
        public ProjectServiceImpl(IUnitOfWork unitOfWork
            , IProjectRepository projectRepository
            , IUserRepository userRepository
            , ApplicationUserManager userManager
            , IUserService userService
            , ITaskService taskService
            , ICommentService commentService)
        {
            _unitOfWork = unitOfWork;
            _projectRepository = projectRepository;
            _userRepository = userRepository;
            _userManager = userManager;
            _userService = userService;
            _taskService = taskService;
            _commentService = commentService;
        }

        #region public methods

        public Result<ProjectViewModel> GetProjectById(Int32 projectId, String requesterUserId)
        {
            var result = new Result<ProjectViewModel>();

            var project = _projectRepository.GetById(projectId);
            if (project == null)
            {
                result.Errors = new List<String> { Messages.NotFoundProject };
                return result;
            }
            if (project.DeleteDateTimeUtc != null)
            {
                result.Errors = new List<String> { Messages.RemovedProject };
                return result;
            }

            var hasAccess =
                project.Users.Any(u => u.Id == requesterUserId) ||
                _userService.IsAdmin(requesterUserId);

            if (hasAccess)
            {
                result.Data = Mapper.Map<ProjectViewModel>(project);
            }
            else
            {
                result.Errors = new List<String> { Messages.AccessDenied };
            }

            return result;
        }

        public Result<IEnumerable<ProjectViewModel>> GetAllProjects(String requesterUserId)
        {
            var result = new Result<IEnumerable<ProjectViewModel>>();

            var hasAccess = _userService.IsAdmin(requesterUserId);

            if (hasAccess)
            {
                var projects = _projectRepository.Query().Where(p => p.DeleteDateTimeUtc == null);
                result.Data = Mapper.Map<IEnumerable<ProjectViewModel>>(projects);
            }
            else
            {
                result.Errors = new List<String> { Messages.AccessDenied };
            }

            return result;
        }

        public Result<IEnumerable<ProjectViewModel>> GetAllUserProjects(String userId, String requesterUserId)
        {
            var result = new Result<IEnumerable<ProjectViewModel>>();

            var user = _userManager.FindByIdAsync(userId).Result;
            if (user == null)
            {
                result.Errors = new List<String> { Messages.NotFoundUser };
                return result;
            }
            if (user.DeleteDateTimeUtc != null)
            {
                result.Errors = new List<String> { Messages.RemovedUser };
                return result;
            }

            var hasAccess = user.Id == requesterUserId || _userService.IsAdmin(requesterUserId);

            if (hasAccess)
            {
                var projects =
                    _projectRepository.Query()
                        .Where(p => p.DeleteDateTimeUtc == null &&
                                    p.Users.Any(u => u.Id == userId));
                result.Data = Mapper.Map<IEnumerable<ProjectViewModel>>(projects);
            }
            else
            {
                result.Errors = new List<String> { Messages.AccessDenied };
            }

            return result;
        }

        public Result<ProjectViewModel> CreateProject(CreateProjectBindingModel projectModel, String requesterUserId)
        {
            var result = new Result<ProjectViewModel>();

            if (!_userService.IsAdmin(requesterUserId))
            {
                result.Errors = new List<String> { Messages.AccessDenied };
                return result;
            }

            if (String.IsNullOrWhiteSpace(projectModel.Title))
            {
                result.Errors = new List<String> { Messages.EmptyProjectTitle };
                return result;
            }

            var project = Mapper.Map<Project>(projectModel);
            project.CreateDateTimeUtc = DateTime.UtcNow;
            _projectRepository.Add(project);

            _unitOfWork.Commit();

            result.Data = Mapper.Map<ProjectViewModel>(project);

            return result;
        }

        public Result<Boolean> UpdateProject(UpdateProjectBindingModel projectModel, String requesterUserId)
        {
            var result = new Result<Boolean>() { Data = false };

            if (!_userService.IsAdmin(requesterUserId))
            {
                result.Errors = new List<String> { Messages.AccessDenied };
                return result;
            }

            if (String.IsNullOrWhiteSpace(projectModel.Title))
            {
                result.Errors = new List<String> { Messages.EmptyProjectTitle };
                return result;
            }

            var project = _projectRepository.GetById(projectModel.Id);
            if (project == null)
            {
                result.Errors = new List<String> { Messages.NotFoundProject };
                return result;
            }
            if (project.DeleteDateTimeUtc != null)
            {
                result.Errors = new List<String> { Messages.RemovedProject };
                return result;
            }

            project.Title = projectModel.Title;
            project.UpdateDateTimeUtc = DateTime.UtcNow;
            _projectRepository.Update(project);

            _unitOfWork.Commit();

            result.Data = true;

            return result;
        }

        public Result<Boolean> DeleteProject(Int32 projectId, String requesterUserId)
        {
            var result = new Result<Boolean>() { Data = false };

            if (!_userService.IsAdmin(requesterUserId))
            {
                result.Errors = new List<String> { Messages.AccessDenied };
                return result;
            }

            var project = _projectRepository.GetById(projectId);
            if (project == null)
            {
                result.Errors = new List<String> { Messages.NotFoundProject };
                return result;
            }

            foreach (var task in project.Tasks)
            {
                _taskService.DeleteTask(task.Id, requesterUserId);
            }
            project.DeleteDateTimeUtc = DateTime.UtcNow;
            _projectRepository.Update(project);

            _unitOfWork.Commit();

            result.Data = true;

            return result;
        }

        #endregion

        #region private fields

        private readonly IUnitOfWork _unitOfWork;
        private readonly IProjectRepository _projectRepository;
        private readonly IUserRepository _userRepository;
        private readonly ApplicationUserManager _userManager;
        private readonly IUserService _userService;
        private readonly ITaskService _taskService;
        private readonly ICommentService _commentService;

        #endregion
    }
}
