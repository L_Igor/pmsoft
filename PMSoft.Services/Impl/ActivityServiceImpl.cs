﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using PMSoft.Services.Interfaces;
using PMSoft.Services.Models;
using PMSoft.Services.Models.BindingModels;
using PMSoft.Services.Models.ViewModels;
using PMSoft.Data.Infrastructure;
using PMSoft.Data.Repository;
using PMSoft.Models;

using Task = PMSoft.Models.Task;

namespace PMSoft.Services.Impl
{
    public class ActivityServiceImpl : IActivityService
    {
        public ActivityServiceImpl(IUnitOfWork unitOfWork
            , IActivityRepository activityRepository
            , ITaskRepository taskRepository
            , ApplicationUserManager userManager
            , IUserService userService)
        {
            _unitOfWork = unitOfWork;
            _activityRepository = activityRepository;
            _taskRepository = taskRepository;
            _userManager = userManager;
            _userService = userService;
        }

        #region public methods

        public Result<ActivityViewModel> GetActivityById(Int32 activityId, String requesterUserId)
        {
            var result = new Result<ActivityViewModel>();

            var activity = _activityRepository.GetById(activityId);
            if (activity == null)
            {
                result.Errors = new List<String> { Messages.NotFoundActivity };
                return result;
            }
            if (activity.DeleteDateTimeUtc != null)
            {
                result.Errors = new List<String> { Messages.RemovedActivity };
                return result;
            }

            if (_HasAccess(activity.Task, requesterUserId))
            {
                result.Data = Mapper.Map<ActivityViewModel>(activity);
            }
            else
            {
                result.Errors = new List<String> { Messages.AccessDenied };
            }

            return result;
        }

        public Result<IEnumerable<ActivityViewModel>> GetAllActivities(String requesterUserId)
        {
            var result = new Result<IEnumerable<ActivityViewModel>>();

            var hasAccess = _userService.IsAdmin(requesterUserId);

            if (hasAccess)
            {
                var activities = _activityRepository.Query().Where(p => p.DeleteDateTimeUtc == null);
                result.Data = Mapper.Map<IEnumerable<ActivityViewModel>>(activities);
            }
            else
            {
                result.Errors = new List<String> { Messages.AccessDenied };
            }

            return result;
        }

        public Result<IEnumerable<ActivityViewModel>> GetAllTaskActivities(Int32 taskId, String requesterUserId)
        {
            var result = new Result<IEnumerable<ActivityViewModel>>();

            var task = _taskRepository.GetById(taskId);
            if (task == null)
            {
                result.Errors = new List<String> { Messages.NotFoundTask };
                return result;
            }
            if (task.DeleteDateTimeUtc != null)
            {
                result.Errors = new List<String> { Messages.RemovedTask };
                return result;
            }

            var isAdminRequesterUser = _userService.IsAdmin(requesterUserId);
            if (_HasAccess(task, requesterUserId))
            {
                var activities =
                    _activityRepository.Query()
                        .Where(p => p.DeleteDateTimeUtc == null &&
                                    p.Task.Id == taskId &&
                                    (p.Task.Project.Users.Any(u => u.Id == requesterUserId) || isAdminRequesterUser));
                result.Data = Mapper.Map<IEnumerable<ActivityViewModel>>(activities);
            }
            else
            {
                result.Errors = new List<String> { Messages.AccessDenied };
            }

            return result;
        }

        public Result<IEnumerable<ActivityViewModel>> GetAllUserActivities(String userId, String requesterUserId)
        {
            var result = new Result<IEnumerable<ActivityViewModel>>();

            var user = _userManager.FindByIdAsync(requesterUserId).Result;
            if (user == null)
            {
                result.Errors = new List<String> { Messages.NotFoundUser };
                return result;
            }
            if (user.DeleteDateTimeUtc != null)
            {
                result.Errors = new List<String> { Messages.RemovedUser };
                return result;
            }

            var hasAccess = user.Id == requesterUserId || _userService.IsAdmin(requesterUserId);

            if (hasAccess)
            {
                var activities =
                    _activityRepository.Query()
                        .Where(p => p.DeleteDateTimeUtc == null &&
                                    p.Task.DeleteDateTimeUtc == null &&
                                    p.Task.Project.DeleteDateTimeUtc == null &&
                                    p.User.Id == userId);
                result.Data = Mapper.Map<IEnumerable<ActivityViewModel>>(activities);
            }
            else
            {
                result.Errors = new List<String> { Messages.AccessDenied };
            }

            return result;
        }

        public Result<ActivityViewModel> CreateActivity(CreateActivityBindingModel activityModel, String requesterUserId)
        {
            var result = new Result<ActivityViewModel>();

            var user = _userManager.FindByIdAsync(activityModel.UserId).Result;
            if (user == null)
            {
                result.Errors = new List<String> { Messages.NotFoundUser };
                return result;
            }
            if (user.DeleteDateTimeUtc != null)
            {
                result.Errors = new List<String> { Messages.RemovedUser };
                return result;
            }
            if (user.Id != requesterUserId)
            {
                result.Errors = new List<String> { Messages.ActionForbidden };
                return result;
            }

            var task = _taskRepository.GetById(activityModel.TaskId);
            if (task == null)
            {
                result.Errors = new List<String> { Messages.NotFoundTask };
                return result;
            }
            if (task.DeleteDateTimeUtc != null)
            {
                result.Errors = new List<String> { Messages.RemovedTask };
                return result;
            }

            if (!_HasAccess(task, requesterUserId))
            {
                result.Errors = new List<String> { Messages.AccessDenied };
                return result;
            }

            var listErrors = new List<String>();

            if (!_IsValidActiveness(activityModel.Activeness))
            {
                listErrors.Add(Messages.ActivityRange);
            }
            if (!_IsStartDateTimeLessEndDateTime(activityModel.StartDateTimeUtc, activityModel.EndDateTimeUtc))
            {
                listErrors.Add(Messages.StartDateTimeLessEndDateTime);
            }
            if (!_IsStartDateTimeAndEndDateTimeLessDateTimeNow(activityModel.StartDateTimeUtc, activityModel.EndDateTimeUtc))
            {
                listErrors.Add(Messages.StartDateTimeAndEndDateTimeLessDateTimeNow);
            }
            if (!_IsValidStartDateTime(activityModel.StartDateTimeUtc))
            {
                listErrors.Add(Messages.ValidStartDateTime);
            }
            if (!_IsValidDurationOfActivity(activityModel.StartDateTimeUtc, activityModel.EndDateTimeUtc))
            {
                listErrors.Add(Messages.ValidDurationOfActivity);
            }

            if (listErrors.Any())
            {
                result.Errors = listErrors;
                return result;
            }

            var activity = Mapper.Map<Activity>(activityModel);
            activity.Task = task;
            activity.User = user;
            activity.CreateDateTimeUtc = DateTime.UtcNow;
            _activityRepository.Add(activity);

            _unitOfWork.Commit();

            result.Data = Mapper.Map<ActivityViewModel>(activity);

            return result;
        }

        public Result<Boolean> UpdateActivity(UpdateActivityBindingModel activityModel, String requesterUserId)
        {
            var result = new Result<Boolean>() { Data = false };

            var activity = _activityRepository.GetById(activityModel.Id);
            if (activity == null)
            {
                result.Errors = new List<String> { Messages.NotFoundActivity };
                return result;
            }
            if (activity.DeleteDateTimeUtc != null)
            {
                result.Errors = new List<String> { Messages.RemovedActivity };
                return result;
            }

            if (!_HasAccess(activity.Task, requesterUserId))
            {
                result.Errors = new List<String> { Messages.AccessDenied };
                return result;
            }

            var listErrors = new List<String>();

            if (!_IsValidActiveness(activityModel.Activeness))
            {
                listErrors.Add(Messages.ActivityRange);
            }
            if (!_IsStartDateTimeLessEndDateTime(activityModel.StartDateTimeUtc, activityModel.EndDateTimeUtc))
            {
                listErrors.Add(Messages.StartDateTimeLessEndDateTime);
            }
            if (!_IsStartDateTimeAndEndDateTimeLessDateTimeNow(activityModel.StartDateTimeUtc, activityModel.EndDateTimeUtc))
            {
                listErrors.Add(Messages.StartDateTimeAndEndDateTimeLessDateTimeNow);
            }
            if (!_IsValidStartDateTime(activityModel.StartDateTimeUtc))
            {
                listErrors.Add(Messages.ValidStartDateTime);
            }
            if (!_IsValidDurationOfActivity(activityModel.StartDateTimeUtc, activityModel.EndDateTimeUtc))
            {
                listErrors.Add(Messages.ValidDurationOfActivity);
            }

            if (listErrors.Any())
            {
                result.Errors = listErrors;
                return result;
            }

            activity.StartDateTimeUtc = activityModel.StartDateTimeUtc;
            activity.EndDateTimeUtc = activityModel.EndDateTimeUtc;
            activity.Activeness = activityModel.Activeness;
            activity.UpdateDateTimeUtc = DateTime.UtcNow;
            _activityRepository.Update(activity);

            _unitOfWork.Commit();

            result.Data = true;

            return result;
        }

        public Result<Boolean> DeleteActivity(Int32 activityId, String requesterUserId)
        {
            var result = new Result<Boolean>() { Data = false };

            var activity = _activityRepository.GetById(activityId);
            if (activity == null)
            {
                result.Errors = new List<String> { Messages.NotFoundActivity };
                return result;
            }
            if (activity.DeleteDateTimeUtc != null)
            {
                result.Errors = new List<String> { Messages.RemovedActivity };
                return result;
            }

            if (!_HasAccess(activity.Task, requesterUserId))
            {
                result.Errors = new List<String> { Messages.AccessDenied };
                return result;
            }

            activity.DeleteDateTimeUtc = DateTime.UtcNow;
            _activityRepository.Update(activity);

            _unitOfWork.Commit();

            result.Data = true;

            return result;
        }

        #endregion

        #region private methods

        private Boolean _HasAccess(Task task, String requesterUserId)
        {
            var requesterUser = _userManager.FindByIdAsync(requesterUserId).Result;
            //var task = _taskRepository.GetById(taskId);

            var hasAccess =
                (
                    requesterUser != null &&
                    task != null && task.DeleteDateTimeUtc == null &&
                    task.Project != null && task.Project.DeleteDateTimeUtc == null &&
                    task.Project.Users.Any(u => u.Id == requesterUser.Id)
                ) ||
                _userService.IsAdmin(requesterUserId);

            return hasAccess;
        }

        private Boolean _IsValidActiveness(Double activeness)
        {
            var MinValueActiveness = 0.0;
            var MaxValueActiveness = 1.0;
            var isValidActiveness = MinValueActiveness <= activeness && activeness <= MaxValueActiveness;

            return isValidActiveness;
        }

        private Boolean _IsStartDateTimeLessEndDateTime(DateTime startDateTimeUtc, DateTime endDateTimeUtc)
        {
            var isStartDateTimeLessEndDateTimeUtc = startDateTimeUtc < endDateTimeUtc;

            return isStartDateTimeLessEndDateTimeUtc;
        }

        private Boolean _IsStartDateTimeAndEndDateTimeLessDateTimeNow(DateTime startDateTimeUtc, DateTime endDateTimeUtc)
        {
            var dateTimeNowUtc = DateTime.UtcNow;
            var isStartDateTimeLessEndDateTimeUtc = startDateTimeUtc < dateTimeNowUtc && endDateTimeUtc <= dateTimeNowUtc;

            return isStartDateTimeLessEndDateTimeUtc;
        }

        private Boolean _IsValidStartDateTime(DateTime startDateTimeUtc)
        {
            var maxTimeToCreateActivity = new TimeSpan(1, 0, 0, 0);
            var isValidStartDateTime = DateTime.UtcNow.Subtract(startDateTimeUtc) <= maxTimeToCreateActivity;

            return isValidStartDateTime;
        }

        private Boolean _IsValidDurationOfActivity(DateTime startDateTimeUtc, DateTime endDateTimeUtc)
        {
            var maxDurationOfActivity = new TimeSpan(0, 30, 0);
            var isValidDurationOfActivity = endDateTimeUtc.Subtract(startDateTimeUtc) <= maxDurationOfActivity;

            return isValidDurationOfActivity;
        }

        #endregion

        #region private fields

        private readonly IUnitOfWork _unitOfWork;
        private readonly IActivityRepository _activityRepository;
        private readonly ITaskRepository _taskRepository;
        private readonly ApplicationUserManager _userManager;
        private readonly IUserService _userService;

        #endregion
    }
}
