﻿using PMSoft.Data.Infrastructure;
using PMSoft.Data.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PMSoft.Models;
using PMSoft.Services.Interfaces;
using PMSoft.Services.Models;
using PMSoft.Services.Models.ViewModels;
using AutoMapper;

namespace PMSoft.Services.Impl
{
    public class CreatorServiceImpl : ICreatorService
    {
        public CreatorServiceImpl(IUnitOfWork unitOfWork
            , ApplicationUserManager userManager
            , ApplicationRoleManager roleManager
            , IUserService userService)
        {
            _unitOfWork = unitOfWork;
            _userManager = userManager;
            _roleManager = roleManager;
            _userService = userService;
        }

        #region public methods

        public Result<Boolean> GrantAdminPermission(String userId, String requesterUserId)
        {
            var result = new Result<Boolean>() { Data = false };

            if (!_userService.IsCreator(requesterUserId))
            {
                result.Errors = new List<String> { Messages.AccessDenied };
                return result;
            }

            var user = _userManager.FindByIdAsync(userId).Result;
            if (user == null)
            {
                result.Errors = new List<String> { Messages.NotFoundUser };
                return result;
            }
            if (user.DeleteDateTimeUtc != null)
            {
                result.Errors = new List<String> { Messages.RemovedUser };
                return result;
            }

            var identityResult = _userManager.AddToRoleAsync(user.Id, UserRoleType.Admin.ToString()).Result;
            if (!identityResult.Succeeded)
            {
                result.Errors = identityResult.Errors;
                return result;
            }

            user.UpdateDateTimeUtc = DateTime.UtcNow;
            identityResult = _userManager.UpdateAsync(user).Result;
            if (!identityResult.Succeeded)
            {
                result.Errors = identityResult.Errors;
                return result;
            }

            result.Data = true;

            return result;
        }

        public Result<Boolean> RevokeAdminPermission(String userId, String requesterUserId)
        {
            var result = new Result<Boolean>() { Data = false };

            if (!_userService.IsCreator(requesterUserId))
            {
                result.Errors = new List<String> { Messages.AccessDenied };
                return result;
            }

            var user = _userManager.FindByIdAsync(userId).Result;
            if (user == null)
            {
                result.Errors = new List<String> { Messages.NotFoundUser };
                return result;
            }
            if (user.DeleteDateTimeUtc != null)
            {
                result.Errors = new List<String> { Messages.RemovedUser };
                return result;
            }

            var identityResult = _userManager.RemoveFromRoleAsync(user.Id, UserRoleType.Admin.ToString()).Result;
            if (!identityResult.Succeeded)
            {
                result.Errors = identityResult.Errors;
                return result;
            }
            user.UpdateDateTimeUtc = DateTime.UtcNow;
            _userManager.UpdateAsync(user);

            result.Data = true;

            return result;
        }

        #endregion

        #region private fields

        private readonly IUnitOfWork _unitOfWork;
        private readonly ApplicationUserManager _userManager;
        private readonly ApplicationRoleManager _roleManager;
        private readonly IUserService _userService;

        #endregion
    }
}
