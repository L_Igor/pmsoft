﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using PMSoft.Services.Interfaces;
using PMSoft.Services.Models;
using PMSoft.Services.Models.BindingModels;
using PMSoft.Services.Models.ViewModels;
using PMSoft.Data.Infrastructure;
using PMSoft.Data.Repository;

using Task = PMSoft.Models.Task;

namespace PMSoft.Services.Impl
{
    public class TaskServiceImpl : ITaskService
    {
        public TaskServiceImpl(IUnitOfWork unitOfWork
            , ITaskRepository taskRepository
            , IProjectRepository projectRepository
            , ApplicationUserManager userManager
            , IUserService userService
            , ICommentService commentService)
        {
            _unitOfWork = unitOfWork;
            _taskRepository = taskRepository;
            _projectRepository = projectRepository;
            _userManager = userManager;
            _userService = userService;
            _commentService = commentService;
        }

        #region public methods

        public Result<TaskViewModel> GetTaskById(Int32 taskId, String requesterUserId)
        {
            var result = new Result<TaskViewModel>();

            var task = _taskRepository.GetById(taskId);
            if (task == null)
            {
                result.Errors = new List<String> { Messages.NotFoundTask };
                return result;
            }
            if (task.DeleteDateTimeUtc != null)
            {
                result.Errors = new List<String> { Messages.RemovedTask };
                return result;
            }

            var hasAccess =
                task.Project.Users.Any(u => u.Id == requesterUserId) ||
                _userService.IsAdmin(requesterUserId);

            if (hasAccess)
            {
                result.Data = Mapper.Map<TaskViewModel>(task);
            }
            else
            {
                result.Errors = new List<String> { Messages.AccessDenied };
            }

            return result;
        }

        public Result<IEnumerable<TaskViewModel>> GetAllTasks(String requesterUserId)
        {
            var result = new Result<IEnumerable<TaskViewModel>>();

            var hasAccess = _userService.IsAdmin(requesterUserId);

            if (hasAccess)
            {
                var tasks = _taskRepository.Query().Where(p => p.DeleteDateTimeUtc == null);
                result.Data = Mapper.Map<IEnumerable<TaskViewModel>>(tasks);
            }
            else
            {
                result.Errors = new List<String> { Messages.AccessDenied };
            }

            return result;
        }

        public Result<IEnumerable<TaskViewModel>> GetAllProjectTasks(Int32 projectId, String requesterUserId)
        {
            var result = new Result<IEnumerable<TaskViewModel>>();

            var project = _projectRepository.GetById(projectId);
            if (project == null)
            {
                result.Errors = new List<String> { Messages.NotFoundProject };
                return result;
            }
            if (project.DeleteDateTimeUtc != null)
            {
                result.Errors = new List<String> { Messages.RemovedProject };
                return result;
            }

            var isAdminRequesterUser = _userService.IsAdmin(requesterUserId);
            var hasAccess = project.Users.Any(u => u.Id == requesterUserId) || isAdminRequesterUser;

            if (hasAccess)
            {
                var tasks =
                    _taskRepository.Query()
                        .Where(p => p.DeleteDateTimeUtc == null &&
                                    p.Project.Id == projectId &&
                                    (p.Project.Users.Any(u => u.Id == requesterUserId) || isAdminRequesterUser));
                result.Data = Mapper.Map<IEnumerable<TaskViewModel>>(tasks);
            }
            else
            {
                result.Errors = new List<String> { Messages.AccessDenied };
            }

            return result;
        }

        public Result<IEnumerable<TaskViewModel>> GetAllUserTasks(String userId, String requesterUserId)
        {
            var result = new Result<IEnumerable<TaskViewModel>>();

            var user = _userManager.FindByIdAsync(userId).Result;
            if (user == null)
            {
                result.Errors = new List<String> { Messages.NotFoundUser };
                return result;
            }
            if (user.DeleteDateTimeUtc != null)
            {
                result.Errors = new List<String> { Messages.RemovedUser };
                return result;
            }

            var hasAccess = user.Id == requesterUserId || _userService.IsAdmin(requesterUserId);

            if (hasAccess)
            {
                var tasks =
                    _taskRepository.Query()
                        .Where(p => p.DeleteDateTimeUtc == null &&
                                    p.Project.DeleteDateTimeUtc == null &&
                                    p.Project.Users.Any(u => u.Id == userId));
                result.Data = Mapper.Map<IEnumerable<TaskViewModel>>(tasks);
            }
            else
            {
                result.Errors = new List<String> { Messages.AccessDenied };
            }

            return result;
        }

        public Result<TaskViewModel> CreateTask(CreateTaskBindingModel taskModel, String requesterUserId)
        {
            var result = new Result<TaskViewModel>();

            if (!_userService.IsAdmin(requesterUserId))
            {
                result.Errors = new List<String> { Messages.AccessDenied };
                return result;
            }

            if (String.IsNullOrWhiteSpace(taskModel.Title))
            {
                result.Errors = new List<String> { Messages.EmptyTaskTitle };
                return result;
            }

            var project = _projectRepository.GetById(taskModel.ProjectId);
            if (project == null)
            {
                result.Errors = new List<String> { Messages.NotFoundProject };
                return result;
            }
            if (project.DeleteDateTimeUtc != null)
            {
                result.Errors = new List<String> { Messages.RemovedProject };
                return result;
            }

            var task = Mapper.Map<Task>(taskModel);
            task.Project = project;
            task.CreateDateTimeUtc = DateTime.UtcNow;
            _taskRepository.Add(task);

            _unitOfWork.Commit();

            result.Data = Mapper.Map<TaskViewModel>(task);

            return result;
        }

        public Result<Boolean> UpdateTask(UpdateTaskBindingModel taskModel, String requesterUserId)
        {
            var result = new Result<Boolean>() { Data = false };

            if (!_userService.IsAdmin(requesterUserId))
            {
                result.Errors = new List<String> { Messages.AccessDenied };
                return result;
            }

            var task = _taskRepository.GetById(taskModel.Id);
            if (task == null)
            {
                result.Errors = new List<String> { Messages.NotFoundTask };
                return result;
            }
            if (task.DeleteDateTimeUtc != null)
            {
                result.Errors = new List<String> { Messages.RemovedTask };
                return result;
            }

            task.Title = taskModel.Title;
            task.Description = taskModel.Description;
            task.UpdateDateTimeUtc = DateTime.UtcNow;
            _taskRepository.Update(task);

            _unitOfWork.Commit();

            result.Data = true;

            return result;
        }

        public Result<Boolean> DeleteTask(Int32 taskId, String requesterUserId)
        {
            var result = new Result<Boolean>() { Data = false };

            if (!_userService.IsAdmin(requesterUserId))
            {
                result.Errors = new List<String> { Messages.AccessDenied };
                return result;
            }

            var task = _taskRepository.GetById(taskId);
            if (task == null)
            {
                result.Errors = new List<String> { Messages.NotFoundTask };
                return result;
            }

            foreach (var comment in task.Comments)
            {
                _commentService.DeleteComment(comment.Id, requesterUserId);
            }
            task.DeleteDateTimeUtc = DateTime.UtcNow;
            _taskRepository.Update(task);

            _unitOfWork.Commit();

            result.Data = true;

            return result;
        }

        #endregion

        #region private fields

        private readonly IUnitOfWork _unitOfWork;
        private readonly ITaskRepository _taskRepository;
        private readonly IProjectRepository _projectRepository;
        private readonly ApplicationUserManager _userManager;
        private readonly IUserService _userService;
        private readonly ICommentService _commentService;

        #endregion
    }
}
